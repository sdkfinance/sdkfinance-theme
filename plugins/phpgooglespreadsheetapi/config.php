<?php
return [
	'OAuth2URL' => [
		'base'     => 'https://accounts.google.com/o/oauth2',
		'auth'     => 'auth', // for Google authorization
		'token'    => 'token', // for OAuth2 token actions
		'redirect' => 'https://sdk.finance',
	],

	'clientID'      => '1014024378334-ikt3vgp2ljnr9ckd4n5fdu06o2msr0rd.apps.googleusercontent.com',
	'clientSecret'  => '34RUHVfzNMxjFuu8zkKvR87o',
	'tokenDataFile' => get_template_directory() . '/plugins/phpgooglespreadsheetapi/.tokendata',
];
