<?php
require_once('base.php');
require_once('googlespreadsheet/api.php');
require_once('googlespreadsheet/api/parser.php');
require_once('googlespreadsheet/api/parser/simpleentry.php');
require_once('googlespreadsheet/cellitem.php');
require_once('oauth2/token.php');
require_once('oauth2/googleapi.php');


class TableGetter extends Base {

  /**
   * @param array  $data
   * @return array
   * @throws Exception
   */
	public function execute( $params = array() ) {

    if ( !array_key_exists('spreadsheetKey', $params) ) {
      return;
    }

    if ( !array_key_exists('worksheetName', $params) ) {
      return;
    }

		// load OAuth2 token data - exit if false
		if (($tokenData = $this->loadOAuth2TokenData()) === false) {
			return;
		}

		// setup Google OAuth2 handler
		$OAuth2GoogleAPI = $this->getOAuth2GoogleAPIInstance();

		$OAuth2GoogleAPI->setTokenData(
			$tokenData['accessToken'],
			$tokenData['tokenType'],
			$tokenData['expiresAt'],
			$tokenData['refreshToken']
		);

		$OAuth2GoogleAPI->setTokenRefreshHandler(function(array $tokenData) {

			// save updated OAuth2 token data back to file
			$this->saveOAuth2TokenData($tokenData);
		});

		$spreadsheetAPI = new GoogleSpreadsheet\API($OAuth2GoogleAPI);

		// fetch all worksheets and display
		$worksheetList = $spreadsheetAPI->getWorksheetList($params['spreadsheetKey']);

    // fetch ID of first worksheet from list
    $worksheetID = array_keys($worksheetList)[0];

    foreach ($worksheetList as $id => $worksheet)
      if ($worksheet['name'] === $params['worksheetName'])
        $worksheetID = $id;

		// fetch worksheet data list and display
		$datalist = $spreadsheetAPI->getWorksheetDataList(
      $params['spreadsheetKey'],
			$worksheetID
		);

		return $datalist;
	}

	private function loadOAuth2TokenData() {

		$tokenDataFile = $this->config['tokenDataFile'];

		if (!is_file($tokenDataFile)) {
			echo(sprintf(
				"Error: unable to locate token file [%s]\n",
				$tokenDataFile
			));

			return false;
		}

		// load file, return data as PHP array
		return unserialize(file_get_contents($tokenDataFile));
	}
}


$TableGetter = new TableGetter(require('config.php'));
