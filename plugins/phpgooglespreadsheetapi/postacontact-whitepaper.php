<?php
require_once('base.php');
require_once('googlespreadsheet/api.php');
require_once('googlespreadsheet/api/parser.php');
require_once('googlespreadsheet/api/parser/simpleentry.php');
require_once('googlespreadsheet/cellitem.php');
require_once('oauth2/token.php');
require_once('oauth2/googleapi.php');


class PostAContactWhitePaper extends Base {


	public function execute( $data = array() ) {

		// load OAuth2 token data - exit if false
		if (($tokenData = $this->loadOAuth2TokenData()) === false) {
			return;
		}

		// setup Google OAuth2 handler
		$OAuth2GoogleAPI = $this->getOAuth2GoogleAPIInstance();

		$OAuth2GoogleAPI->setTokenData(
			$tokenData['accessToken'],
			$tokenData['tokenType'],
			$tokenData['expiresAt'],
			$tokenData['refreshToken']
		);

		$OAuth2GoogleAPI->setTokenRefreshHandler(function(array $tokenData) {

			// save updated OAuth2 token data back to file
			$this->saveOAuth2TokenData($tokenData);
		});

		$spreadsheetAPI = new GoogleSpreadsheet\API($OAuth2GoogleAPI);

		// fetch all available spreadsheets and display
		$spreadsheetList = $spreadsheetAPI->getSpreadsheetList();
		//print_r($spreadsheetList);

		if (!$spreadsheetList) {
			echo("Error: No spreadsheets found\n");
			exit();
		}

		// fetch key of first spreadsheet
		//$spreadsheetKey = array_keys($spreadsheetList)[0];
    $spreadsheetKey = '1l4wM_1HXz7XOjcDXf_yXrbLz07-2a5A3pvAn-N4c9E0';

		// fetch all worksheets and display
		$worksheetList = $spreadsheetAPI->getWorksheetList($spreadsheetKey);
		//print_r($worksheetList);

		// fetch ID of 2nd worksheet from list
		$worksheetID = array_keys($worksheetList)[2];

		// fetch worksheet data list and display
		$spreadsheetAPI->getWorksheetDataList(
			$spreadsheetKey,
			$worksheetID
		);

		// fetch worksheet cell list and display
		$cellList = $spreadsheetAPI->getWorksheetCellList(
			$spreadsheetKey,
			$worksheetID, array(
				"columnStart" => 2,
				'columnEnd' => 10
      )
		);

		//print_r($cellList);

		// update content of first cell
		$cellIndex = array_keys($cellList)[0];

		$spreadsheetAPI->updateWorksheetCellList(
			$spreadsheetKey,
			$worksheetID,
			$cellList
		);

		// add data row to worksheet
		$dataList = $spreadsheetAPI->getWorksheetDataList($spreadsheetKey,$worksheetID);

		$rowData = [];
		foreach ($dataList['headerList'] as $index => $headerName) {
			$rowData[$headerName] = $data[$index];
		}

		$spreadsheetAPI->addWorksheetDataRow($spreadsheetKey,$worksheetID,$rowData);

    return [
      'status' => '200',
      'worksheetList' => $worksheetList
    ];
	}

	private function loadOAuth2TokenData() {

		$tokenDataFile = $this->config['tokenDataFile'];

		if (!is_file($tokenDataFile)) {
			echo(sprintf(
				"Error: unable to locate token file [%s]\n",
				$tokenDataFile
			));

			return false;
		}

		// load file, return data as PHP array
		return unserialize(file_get_contents($tokenDataFile));
	}
}


$PostAContactWhitePaper = new PostAContactWhitePaper(require('config.php'));
