<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">
<head>

<!-- Meta view -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">

<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - AdWords: 955523985 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-955523985"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

 gtag('config', 'AW-955523985');
</script>

<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-59849201-3', 'auto');
	ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window,document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '505932913137924');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=505932913137924&ev=PageView
&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

</head>
<body>

<?php get_template_part( 'views/core/cookie-policy' ); ?>

<div id="wrapper">
<div id="body-class"  <?php body_class(); ?>>

	<?php
	/**
	 * Header parametres
	 */
	$d             = array();
	$classes       = array();
	$classes[]     = 'header-primary';

	$classes[]     = is_front_page() ? "position_overlay" : ''; 
	$classes[]     = 'js-sticky'; 
	$classes       = count($classes) > 0 ? 'class="' . generate_classlist($classes) . '"' : '';
	?>

	<header <?=$classes;?>>
		<div class="header-primary__box box">

			<?php if ( ! is_front_page() ) : ?>
				<a href="<?php bloginfo('url'); ?>" class="header-primary--logo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sdk-finance-logo.png" alt="SDK.finance Logotype" />
				</a>
			<?php else : ?>
				<div class="header-primary--logo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sdk-finance-logo.png" alt="SDK.finance Logotype" />
				</div>
			<?php endif; ?>

      <?php if ( get_field( 'header-footer-type' ) !== 'mini' ) : ?>
        <div class="header-primary--menu">
          <?php
          /**
           * Return primary navigation
           */
          wp_nav_menu( array(
            'theme_location' => 'primary'
          ) );
          ?>
        </div>

        <?php if ( false ) : ?>

          <div class="header-primary--search">
            <a href="#." class="header-primary--search__label"><i class="fa fa-search" aria-hidden="true"></i></a>
          </div>

        <?php endif; ?>

        <div class="header-primary--actions">
          <?php
          /**
           * Return primary navigation
           */
          wp_nav_menu( array( 'theme_location' => 'action', 'fallback_cb' => '' ) );
          ?>
        </div>

        <a href="javascript:" class="header-primary--burger"><div></div></a>
      <?php endif; ?>
		</div>
	</header>

<main>