<?php
/**
 * Page 404
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

get_header();
?>

<div id="primary-content" class="primary-content__wrapper">
	<div class="section paddings_large color_light">
		<div class="rows">
			<div style="" class="row box box-inner layout_grid width_one-two position_center cols_placement_middle cols_margins_none cols_align_left cols_count_1">
				<div class="row--column wysiwyg width_auto">
					<h2 style="" class="is-brand-back text-align_center auto">Oops!<br />It seems like you have turned over <nobr>to 404 page!</nobr></h2>
					<p style="margin-top:2.5rem;" class="is-brand-back text-align_center auto">Unfortunately, the page you have been requested doesn't exist anymore. Would be better to come back to <a href="/">Home page</a></p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
