<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

get_header();

	if ( is_singular() ) :
		
		if ( have_posts() ) : 
			while ( have_posts() ) : 
				the_post(); 

				/**
				 * Page's title area
				 */
				if ( ! is_front_page() && get_field( 'show-heroarea' ) != 'hide' )
					get_template_part( 'template-parts/heroarea' );

				/**
				 * Page's content
				 */
				get_template_part( 'template-parts/primary-content' );

			endwhile;
		endif;

	elseif ( is_archive() || is_home() || is_author() ) :

		get_template_part( 'template-parts/heroarea' );
		?>

		<div class="section paddings_middle box box-inner layout_sidebar">
			<div class="archive-loop">
				<?php
				/**
				 * Loop of posts
				 */
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/article' );
					endwhile;
				endif;

				/**
				 * Pagination
				 */
				echo wp_corenavi();
				?>
			</div>

			<?php get_sidebar(); ?>
		</div>
		
		<?php
	endif;

get_footer();
