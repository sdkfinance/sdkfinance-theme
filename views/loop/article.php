<?php
/**
 * View: Article
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div class="article">
	<?php if ( $d['thumb'] ) : ?>
		<a href="<?=$d['url'];?>" class="article--thumb"><?=$d['thumb'];?></a>
	<?php endif; ?>

	<div class="article--meta">
		<h2 class="h6"><a href="<?=$d['url'];?>" class="article--meta__title"><?=$d['title'];?></a></h2>
		<p class="article--meta__info"><?=$d['date'];?></p>
	</div>

	<div class="article--content wysiwyg"><p><?=$d['content'];?></p></div>

	<div class="article--footer">
		<a href="<?=$d['url'];?>" class="button">Read more</a>
	</div>
</div>
