<?php
/**
 * View: Cases slider
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div 
	class="section paddings_middle color_light"
	style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/bg-light.jpg);">

	<div class="rows">
		<div class="row box box-inner wow fadeIn layout_grid width_default position_center cols_placement_middle cols_margins_none cols_align_left cols_count_1">
			<div class="row--column wysiwyg width_auto">
				<h2 class="is-brand-back text-align_center auto">Use-cases</h2>
			</div>		
		</div>

		<div class="row row-type_cases box box-inner wow fadeIn layout_grid width_default position_center cols_placement_top cols_margins_normal" style="margin-top:3rem;">

			<?php 
			/**
			 *  Loop posts
			 */
			$query = new WP_Query( array( 
				'post_type'    => 'use-cases'
			) );

			while ( $query->have_posts() ) :
				$query->the_post();

				$img  = get_field( 'scheme-img' );
				$desc = get_field( 'scheme-desc' );

				if ( $img && $desc ) :
				 	?>

					<div class="row--column">
						<div class="image-component">	
							<img src="<?=$img;?>" class="image-component--img" alt="">
						</div>	

						<div class="wysiwyg"><?=$desc;?></div>	
					</div>

					<?php
				endif;
			endwhile;
		    wp_reset_postdata();
			?>

		</div>
		
		<div class="row box box-inner wow fadeIn layout_grid width_default position_center cols_placement_middle cols_margins_none cols_align_left cols_count_1" style="margin-top:3rem;">
			<div class="row--column wysiwyg width_auto">
				<div class="wysiwyg">
					<p style="text-align: center;">
						<a href="http://sdk.finance/use-cases/implemented-on-sdk-finance" class="button">More Use cases</a>
					</p>
				</div>	
			</div>
		</div>
	</div>
</div>


