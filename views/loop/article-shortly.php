<?php
/**
 * View: Article shortly
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div class="article layout_shourtly">
	<?php if ( $d['thumb'] ) : ?>
		<a href="<?=$d['url'];?>" class="article--thumb"><?=$d['thumb'];?></a>
	<?php endif; ?>

	<div class="article--meta">
		<h6><a href="<?=$d['url'];?>" class="article--meta__title"><?=$d['title'];?></a></h6>
		<p class="article--meta__info"><?=$d['date'];?></p>
	</div>
</div>
