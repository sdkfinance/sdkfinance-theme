<?php
/**
 * View: Image
 *
 * @todo svg animation
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<?=$d['open'];?>
	<img src="<?=$d['image']['url'];?>" class="image-component--img" alt="" />
<?=$d['close'];?>