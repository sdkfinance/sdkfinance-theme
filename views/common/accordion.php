<?php
/**
 * View: Accorion
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>

	<?php foreach ( $d['content'] as $row ) : ?>

		<div class="accorion--item">
			<h6 class="accordion--title">
				<a href="javascript:" data-icon="+" class="accordion--title__link"><?=$row['headline'];?></a>
			</h6>

			<div class="accordion--content">
				<div class="accordion--content__inner">
					<div class="accordion--content__box box box-inner wysiwyg">
						<?=$row['content'];?>
					</div>
				</div>
			</div>
		</div>

	<?php endforeach; ?>

</div>