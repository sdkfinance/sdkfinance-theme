<?php
/**
 * View: Button
 *
 * @var $d
 * @var $classes
 * @var $attrs
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<a href="#." class="button">Button</a>