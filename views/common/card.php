<?php
/**
 * View: Card
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div class="cards-list--item">
	<img class="cards-list--item__icon" src="<?=$d['icon']?>" alt="">
	<div class="cards-list--content">
		<div class="cards-list--core">
			<div class="cards-list--core__general"><?=$d['title']?></div>
			<div class="cards-list--core__sub"><?=$d['sub-title']?></div>
		</div>

		<div class="cards-list--details">
			<div class="cards-list--details__general"><?=$d['details']?></div>
			<div class="cards-list--details__sub"><?=$d['sub-details']?></div>
		</div>
	</div>
</div>