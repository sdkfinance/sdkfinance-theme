<?php
/**
 * View: Logos gallery
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>
    <?php foreach ( $d['gallery'] as $image ) : ?>
		    <div class="logos-gallery__item">
          	<img src="<?=$image['url'];?>" alt="<?=$image['alt'];?>" />
      	</div>
  	<?php endforeach; ?>
</div>