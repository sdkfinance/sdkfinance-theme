<?php
/**
 * View: Video Previews
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div class="video-previews--item">
	<div class="video-previews--item__thumb">
		<?php get_template_part( 'template-parts/components/video' ); ?>
	</div>
	<div class="video-previews--item__meta wysiwyg"><?=$d['content'];?></div>
</div>