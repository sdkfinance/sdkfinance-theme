<?php
/**
 * View: Visualized list
 *
 * @var $d  
 * @var $class   (string)
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<li class="visualized-list--item">
	
	<?php 
	if ( array_key_exists( 'icon', $d ) ):
		?>

		<div class="visualized-list--item__icon">
			<img src="<?=$d['icon'];?>" alt='' />
		</div>

		<?php
	endif;
	?>

	<div class="visualized-list--item__textarea"><?=$d['textarea'];?></div>

	<?php
	if ( array_key_exists( 'sublist', $d ) ) :
		?>
		
		<ul class="visualized-list--sub">

			<?php
			foreach ( $d['sublist'] as $sub ) :
				?>
		
				<li class="visualized-list--sub__textarea"><?=$sub['textarea'];?></li>

				<?php
			endforeach;
			?>

		</ul>

		<?php
	endif; 
	?>
</li>