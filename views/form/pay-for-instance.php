<?php
/**
 * Form view: Email
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<form <?=$attrs;?> enctype="multipart/form-data">
	<div class="form-body">
		<div class="form-field">
			<input type="text" name="email" placeholder="example@mail.com" class="valid-email" />
			<p class="form-field_error"></p>
		</div>
		<div class="form-field">
			<input type="text" name="brand" placeholder="brand name" />
			<div class="form-field_label-end">.sdk.finance</div>
			<p class="form-field_error"></p>
		</div>
		<div class="form-field">
      <label class="file-upload">
        <div class="file-upload__btn">Upload logotype</div>
        <div class="file-upload__label">No logo choosen</div>
        <input type="file" name="logotype" />
      </label>
			<p class="form-field_error"></p>
		</div>
	</div>
	<div class="form-hidden">
		<input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="KZ9AD8DDJ89B6">
	</div>
	<div class="form-footer">
		<div class="form-submit">
			<button type="submit" class="form-submit_btn button"><?=$d['submit'];?></button>
		</div>
	</div>
</form>