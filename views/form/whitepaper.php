<?php
/**
 * Form view: Whitepaper
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<form <?=$attrs;?>>
  <div class="form-body">
    <div class="form-field">
      <input type="text" name="first" placeholder="First Name" />
      <p class="form-field_error"></p>
    </div>

    <div class="form-field">
      <input type="text" name="last" placeholder="Last Name" />
      <p class="form-field_error"></p>
    </div>

    <div class="form-field">
      <input type="text" name="email" placeholder="example@mail.com" class="valid-email" />
      <p class="form-field_error"></p>
    </div>

    <div class="form-field">
      <input type="text" name="company" placeholder="Company" />
      <p class="form-field_error"></p>
    </div>

    <div class="form-field">
      <input type="text" name="role" placeholder="Role" />
      <p class="form-field_error"></p>
    </div>

    <div class="form-field">
      <input type="text" name="phone" placeholder="Business phone" class="valid-phone" />
      <p class="form-field_error"></p>
    </div>
  </div>
  <div class="form-footer">
    <div class="form-submit">
      <button type="submit" class="form-submit_btn button"><?=$d['submit'];?></button>
    </div>
    <?php if ($d['disclaimer']) : ?>
     <p class="form-disclaimer"><?=$d['disclaimer'];?></p>
    <?php endif; ?>
  </div>
</form>