<?php
/**
 * Form view: Email
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<form <?=$attrs;?>>
	<div class="form-body">
		<div class="form-field">
			<input type="text" name="email" placeholder="example@mail.com" class="valid-email" />
			<p class="form-field_error"></p>
		</div>
	</div>
	<div class="form-footer">
		<div class="form-submit">
			<button type="submit" class="form-submit_btn button"><?=$d['submit'];?></button>
		</div>
	</div>
</form>