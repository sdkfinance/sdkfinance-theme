<?php
/**
 * Cookie notification
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


if ( !isset($_COOKIE['user-approve-cookies']) ) :
	?>

	<div id="cookies-about" class="cookies-about">
	    <div class="cookies-about__box box">
	        <div class="cookies-about__body">
	            <div class="cookies-about__msg">
                <p>
                  <?php _e( 'We use cookies to enhance your experience. I accept use of my cookies on this website.' ); ?>
                  <a href="<?php bloginfo('url'); ?>/cookie-policy/" target="_blank"><?php _e( 'More about Cookie Policy'); ?></a>
                </p>
                <p>
	                <?php _e( 'We updated our Terms of Service, Privacy Policy. You need to accept our Privacy Policy before using this website.' ); ?>
	                <a href="<?php bloginfo('url'); ?>/terms-and-conditions/" target="_blank"><?php _e( 'More about Privacy Policy'); ?></a>
                </p>
	            </div>
	            <button id="user-approve-cookies" class="cookies-about__button"><?php _e( 'Ok'); ?></button>
	        </div>
	    </div>
	</div>
	
	<?php
endif;
?>