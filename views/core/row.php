<?php
/**
 * View: Row
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>

	<?php 
	/**
	 * Run functions after open tag.
	 * 
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'after_open_row_tag' ); 
	remove_all_actions( 'after_open_row_tag');


	/**
	 * Get columns.
	 * Each columns can have any inner components.
	 */
	if ( have_rows( 'columns' ) ) :
		while ( have_rows( 'columns' ) ) :
			$the_column  = the_row();
			$width_class = get_sub_field( 'width' ) ? 'width_' . get_sub_field( 'width' ) : '';
			?>

			<div class="row--column wysiwyg <?=$width_class;?>">
				
				<?php 
				/**
				 * Loop components
				 */
				if ( have_rows( 'components' ) ) :
					while ( have_rows( 'components' ) ) :
						$the_component = the_row();
						$layout        = get_row_layout();

						get_template_part( "template-parts/components/$layout" );
					endwhile;
				endif;
				?>	

			</div>
	
			<?php
		endwhile;
	endif;


	/**
	 * Run functions after open tag.
	 *
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'after_open_row_tag' ); 
	remove_all_actions( 'after_open_row_tag');
	?>

</div>