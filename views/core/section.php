<?php
/**
 * View: Section
 *
 * @var $the_section_row - variable that contains section's subfields data 
 * @var $attrs
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

/**
 * Run functions after open tag.
 * For exampe it can be used to video background
 * and border divider.
 *
 * After completing assigned functions
 * that acton will be cleaned up except
 * the functions under 'priority 10'
 */
do_action( 'before_open_section_tag', $the_section_row );
remove_all_actions( 'before_open_section_tag' );
?>

<div <?=$attrs;?>>
	
	<?php 
	/**
	 * Run functions after open tag.
	 * For exampe it can be used to video background 
	 * and border divider.
	 *
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'after_open_section_tag', $the_section_row ); 
	remove_all_actions( 'after_open_section_tag' );


	/**
	 * Get row model data and loop views.
	 *
	 * Now, let's check are there any rows in database.
	 */		
	if ( have_rows( 'rows' ) ) :
		?>

		<div class="rows">

			<?php
			/**
			 * Loop rows
			 */
			while ( have_rows( 'rows' ) ) :
				$the_row = the_row();
				$layout  = get_row_layout();
				
				get_template_part( "template-parts/$layout" );
			endwhile; 
			?>

		</div>

		<?php
	endif;


	/**
	 * Run functions before close tag.
	 * For exampe it can be used to border divider
	 *
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'before_close_section_tag', $the_section_row );
	remove_all_actions( 'before_close_section_tag' );
	?>

</div>