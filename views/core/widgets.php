<?php
/**
 * View: Widgets
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>

	<?php 
	/**
	 * Run functions after open tag.
	 * 
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'after_open_widgets_tag' );
	remove_all_actions( 'after_open_widgets_tag');


	/**
	 * Get columns.
	 * Each columns can have any inner components.
	 */
  while ( have_rows( 'widgets' ) ) :
    the_row();
    $layout = get_row_layout();
    ?>

    <div class="widget wysiwyg">
      <?php get_template_part( "template-parts/components/$layout" ); ?>
    </div>

    <?php
  endwhile;


	/**
	 * Run functions after open tag.
	 *
	 * After completing assigned functions 
	 * that acton will be cleaned up except 
	 * the functions under 'priority 10'
	 */
	do_action( 'after_open_widgets_tag' );
	remove_all_actions( 'after_open_widgets_tag');
	?>

</div>