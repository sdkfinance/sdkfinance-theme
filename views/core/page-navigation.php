<?php
/**
 * Component: Page Navigation
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>
	<div class="page-navigation__wrap box">
	    <?php foreach ( $d['content'] as $label => $target ) : ?>
			<a href="<?=$target?>" class="page-navigation__item scroll-href"><?=$label;?></a>
    	<?php endforeach; ?>
	</div>
</div>
