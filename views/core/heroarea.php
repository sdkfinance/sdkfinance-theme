<?php
/**
 * View: Heroarea
 *
 * @var $classes
 * @var $attrs
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div <?=$attrs;?>>
	<div class="heroarea--box box box-inner">
		<?=$d['title'];?>
	</div>
</div>