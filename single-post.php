<?php
/**
 * The single article template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

get_header();

	if ( have_posts() ) : 
		while ( have_posts() ) : 
			the_post(); 

			// Additional data
			$d          = array();
			$d['thumb'] = get_the_post_thumbnail( $post->ID, '1150-size' );

			// incklude hero area template
			if ( get_field( 'show-heroarea' ) != 'hide' )
				get_template_part( 'template-parts/heroarea' );
			?>

			<div class="section paddings_middle box box-inner">
				<div class="single-area">
          <div class="wysiwyg"><a href="/blog/">&#8592; Go back to Blog</a></div>

					<?php 
					/**
					  * Include sidebar just for mobile 
					  */ 
					if ( wp_is_mobile() ) :
						?>

						<div class="single-area--sidebar"><?php get_sidebar( 'single' ); ?></div>
						
						<?php
					endif; 
					?>

					<div class="single-area--head">
						<h1 class="single-area--head__title"><?php the_title(); ?></h1>
					</div>

					<?php 
					if ( $d['thumb'] ) :
						?>

						<div class="single-area--thumb"><?=$d['thumb'];?></div>

						<?php
					endif;
					?>

					<div class="single-area--body">
						<?php
						/**
						  * Include sidebar just for desktop
						  */ 
						if ( ! wp_is_mobile() ) :
							?>

							<div class="single-area--sidebar area--sidebar"><?php get_sidebar( 'single' ); ?></div>
							
							<?php
						endif;
						?>

						<div class="single-area--content wysiwyg"><?php the_content(); ?></div>
					</div>
				</div>
			</div>

			<?php
			get_template_part( 'template-parts/components/accordion', 'post' );
			get_template_part( 'template-parts/components/related', 'articles' );
		endwhile;
	endif;

get_footer();
