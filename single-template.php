<?php
/**
 * The single template page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

get_header();

	if ( have_posts() ) : 
		while ( have_posts() ) : 
			the_post(); 

			get_template_part( 'template-parts/section-template' );
		endwhile;
	endif;

get_footer();
