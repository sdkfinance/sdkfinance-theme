<?php  
/**
 * The main footer file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */
?>

</main>

<footer class="footer-primary">
    <div class="footer-primary__box box">
        <?php if ( get_field( 'header-footer-type' ) !== 'mini' ) : ?>
          <div class="footer-primary__group">
            <div class="footer-primary__col">
                <?php 
                /**
                 * Return primary navigation
                 */
                wp_nav_menu( array( 
                    'theme_location' => 'footer-1' 
                ) ); 
                ?>
            </div>

            <div class="footer-primary__col">
                <?php 
                /**
                 * Return primary navigation
                 */
                wp_nav_menu( array( 
                    'theme_location' => 'footer-2' 
                ) ); 
                ?>
            </div>

            <div class="footer-primary__col">
                <?php 
                /**
                 * Return primary navigation
                 */
                wp_nav_menu( array( 
                    'theme_location' => 'footer-3' 
                ) ); 
                ?>
            </div>

            <div class="footer-primary__col">
                <?php 
                /**
                 * Return primary navigation
                 */
                wp_nav_menu( array( 
                    'theme_location' => 'footer-4' 
                ) ); 
                ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="footer-primary__copyright">
            <p>2013-<?php echo date('y'); ?> © SDK.finance</p>
            
            <?php 
            /**
             * Return primary navigation
             */
            if (get_field( 'header-footer-type' ) !== 'mini') :
              wp_nav_menu( array(
                  'theme_location' => 'footer-5'
              ) );
            endif;
            ?>
        </div>
    </div>
</footer>

</div>
</div>

<?php 
/**
 * Load the spiner which is appear 
 * while the pages are switching 
 * between each other
 */
get_template_part( 'views/common/spiner' ); 

wp_footer(); 
?>
</body>
</html>
