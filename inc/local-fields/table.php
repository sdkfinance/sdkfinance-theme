<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


acf_add_local_field_group(array (
	'key' => 'table_596506937549f',
	'title' => 'Table params',
	'fields' => array (
		array (
			'key'   => 'table_5964ffd8d9950',
			'label' => 'Key',
			'name'  => 'table-key',
			'type'  => 'text',
      'required' => 1
		)
	),
	'location' => array (
		array (
			array (
				'param'    => 'post_type',
				'operator' => '==',
				'value'    => 'table',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

?>