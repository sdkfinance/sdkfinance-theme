<?php 
/**
 * ACF local fields API
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


function get_form_local_field( $id = '1234QWERasdf' ) {
  return array(
    array (
      'key' => $id . '_Q50080103W4',
      'label' => 'Content',
      'name' => 'tab1',
      'type' => 'tab',
      'required' => ''
    ),
    array (
      'key' => $id . '_Q50081011W5',
      'label' => 'Form type',
      'name' => 'type',
      'type' => 'select',
      'choices' => array(
        'email' => 'Email',
        'brand' => 'Email and Brand',
        'pay-for-instance' => 'Pay for instance',
        'whitepaper' => 'Whitepaper',
      ),
      'wrapper' => array(
        'width' => 25
      ),
      'default_values' => array( 'email' => 'Email' )
    ),
    array (
      'key' => $id . '_Q50081047W9',
      'label' => 'Submit label',
      'name' => 'submit-label',
      'type' => 'text',
      'wrapper' => array(
        'width' => 25
      ),
      'placeholder' => 'Submit'
    ),
    array (
      'key' => $id . '_Q50081156W0',
      'label' => 'Form ID',
      'name' => 'id',
      'type' => 'text',
      'wrapper' => array(
        'width' => 25
      ),
      'placeholder' => 'separate-by-dash',
      'required'  => 1,
      'maxlength' => 20
    ),
    array (
      'key' => $id . '_Q50080265W1',
      'label' => 'Confirmation page',
      'name' => 'confirmation-page',
      'type' => 'page_link',
      'wrapper' => array(
        'width' => 25
      )
    ),
    array (
      'key' => $id . '_Q5008z0d7r9',
      'label' => 'Disclaimer',
      'name' => 'disclaimer',
      'type' => 'text',
      'wrapper' => array(
        'width' => 100
      ),
      'conditional_logic' => array(
        array(
          array(
            'field' => $id . '_Q50081011W5',
            'operator' => '==',
            'value' => 'whitepaper',
          )
        )
      )
    ),
    array (
      'key' => $id . '_Q50081020W6',
      'name' => 'tab2',
      'label' => 'Settings',
      'type' => 'tab',
      'required' => ''
    ),
    array (
      'key' => $id . '_Q50081039W7',
      'name' => 'margin_top',
      'label' => 'Margin top',
      'type' => 'number',
      'required' => '',
      'wrapper' => array (
        'width' => 25
      ),
      'append' => 'px'
    ),
    array (
      'key' => $id . '_Q50081048W8',
      'name' => 'margin_bottom',
      'label' => 'Margin bottom',
      'type' => 'number',
      'required' => '',
      'wrapper' => array (
        'width' => 25
      ),
      'append' => 'px'
    ),
  );
}

/**
 * Generates Row local fields array
 */
function get_row_local_field( $id = '1234QWERasdf' ) {
	return array (
		array (
			'key' => $id . '_12309zcd9as51',
			'label' => 'Columns',
			'name' => 'columns-tab',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $id . '_596512d9952',
			'label' => 'Columns',
			'name' => 'columns',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'button_label' => 'Add Column',
			'layout' => 'block',
			'sub_fields' => array (
				array(
					'key' => $id . '_123o9Gcd8gX55',
					'name' => 'tab1',
					'label' => 'Columns',
					'type' => 'tab',
					'required' => ''
				),
				array (
					'key' => $id . '_5123eO2d795Z',
					'label' => 'Components',
					'name' => 'components',
					'type' => 'flexible_content',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'button_label' => 'Add Component',
					'min' => '',
					'max' => '',
					'layouts' => array (
						array (
							'key' => $id . '_Q50081185Q2',
							'name' => 'title',
							'label' => 'Title',
							'display' => 'block',
							'sub_fields' => array(
								array (
									'key' => $id . '_jGq081185Q2',
									'label' => 'Content',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_qS0081185o2',
									'label' => 'Title',
									'name' => 'title',
									'type' => 'text',
									'required' => 0,
									'formatting' => 'none',
								),
								array (
									'key' => $id . '_jGq081185Q3',
									'label' => 'Settings',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jGq0811RMa11',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa21',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_qS008ii85o2',
									'label' => 'SEO Tag',
									'name' => 'tag',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array (
										'p'  => 'Paragraph',
										'h1' => 'H1',
										'h2' => 'H2',
										'h3' => 'H3',
										'h4' => 'H4',
										'h5' => 'H5',
										'h6' => 'H6',
									),
									'default_value' => 'p'
								),
								array (
									'key' => $id . '_qS008ist5o2',
									'label' => 'Styling',
									'name' => 'styling',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array (
										'h1' => 'H1',
										'h2' => 'H2',
										'h3' => 'H3',
										'h4' => 'H4',
										'h5' => 'H5',
										'h6' => 'H6',
									),
									'default_value' => '',
                  'allow_null' => true
								),
								array (
									'key' => $id . '_qS008ii1So2',
									'label' => 'Color',
									'name' => 'color',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array (
										'is-dark'  => 'Dark',
										'is-light' => 'Light',
										'is-gray' => 'Gray',
										'is-brand-gradient' => 'Gradient',
										'is-brand-base' => 'Base',
										'is-brand-base-light' => 'Light Base',
										'is-brand-back' => 'Back color',
										'is-brand-back-light' => 'Back color',
										'is-brand-back-medium' => 'Back medium',
									),
									'default_value' => 'is-dark'
								),
								array (
									'key' => $id . '_qS008i11So1',
									'label' => 'Align',
									'name' => 'align',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array (
										'auto' => 'Auto',
										'text-align_left'  => 'Left',
										'text-align_center' => 'Center',
										'text-align_right' => 'Right',
										'text-align_justify' => 'Justify',
									),
									'default_values' => array ( 0 => 0 )
								),
								array (
									'key' => $id . '_qS008i43So2',
									'label' => 'Weight',
									'name' => 'weight',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array (
										'auto'  => 'Auto',
										'font_light' => 'Light',
										'font_regular' => 'Regular',
										'font_medium' => 'Medium',
										'font_bold' => 'Bold',
									),
									'default_values' => array ( 'auto' )
								),
							),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_Q50081185E2',
							'name' => 'wysiwyg',
							'label' => 'WYSIWYG',
							'display' => 'block',
							'sub_fields' => array(
								array (
									'key' => $id . '_jGQ1821RMa11',
									'label' => 'Content',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_58ac3e9ce90b7',
									'label' => 'Content',
									'name' => 'content',
									'type' => 'wysiwyg',
									'required' => 0,
									'conditional_logic' => 0,
									'default_value' => '',
									'tabs' => 'all',
									'toolbar' => 'full',
									'media_upload' => 1,
								),
								array (
									'key' => $id . '_jGq0821RMa12',
									'name' => 'tab2',
									'label' => 'Settings',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jGq0811RMa12',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa22',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_Q50080294W3',
							'label' => 'Form',
							'name' => 'form',
							'display' => 'block',
							'sub_fields' => get_form_local_field($id),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_Q5a08ss85Z2',
							'name' => 'video',
							'label' => 'oEmbed Video',
							'display' => 'block',
							'sub_fields' => array(
								array (
									'key' => $id . '_jGq2821RM323',
									'name' => 'tab1',
									'label' => 'Content',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_5829D67d25',
									'label' => 'Video',
									'name' => 'video',
									'type' => 'oembed',
									'required' => '',
								),
								array (
									'key' => $id . '_jGq2821RM313',
									'name' => 'tab2',
									'label' => 'Settings',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jGq0811RMa13',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa23',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_Q5a08sA85I2',
							'name' => 'cards',
							'label' => 'Cards',
							'display' => 'block',
							'sub_fields' => array(
								array (
									'key' => $id . '_jG32821RM414',
									'name' => 'tab1',
									'label' => 'Content',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_Q5aq8sA85I0',
									'label' => 'Cards list',
									'name' => 'cards',
									'type' => 'repeater',
									'required' => 0,
									'button_label' => 'Add Card',
									'layout' => 'block',
									'sub_fields' => array (
										array (
											'key' => $id . '_jGq081185q2',
											'label' => 'Content',
											'name' => 'tab1',
											'type' => 'tab',
											'required' => ''
										),
										array (
											'key' => $id . '_Q5aq8sA85I1',
											'label' => 'Icon',
											'name' => 'icon',
											'type' => 'image',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => 25,
											),
											'return_format' => 'url',
											'preview_size' => 'thumbnail',
											'library' => 'all',
										),
										array (
											'key' => $id . '_Q5aq8sA85I2',
											'label' => 'Title',
											'name' => 'title',
											'type' => 'text',
											'required' => 0,
											'formatting' => 'none',
											'wrapper' => array (
												'width' => '30'
											),
										),
										array (
											'key' => $id . '_Q5aq8sA85I3',
											'label' => 'Sub title',
											'name' => 'sub-title',
											'type' => 'text',
											'required' => 0,
											'formatting' => 'none',
											'wrapper' => array (
												'width' => '45'
											),
										),
										array (
											'key' => $id . '_Q5aq8sA85I5',
											'label' => 'Details',
											'name' => 'details',
											'type' => 'text',
											'required' => 0,
											'formatting' => 'none',
											'wrapper' => array (
												'width' => '30'
											),
										),
										array (
											'key' => $id . '_Q5aq8sA85I6',
											'label' => 'Sub details',
											'name' => 'sub-details',
											'type' => 'text',
											'required' => 0,
											'formatting' => 'none',
											'wrapper' => array (
												'width' => '70'
											),
										),
									)
								),
								array (
									'key' => $id . '_jGq2821RM314',
									'name' => 'tab2',
									'label' => 'Settings',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jGq0811RMa14',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa24',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_q5a09sa85z2',
							'name' =>'video-previews',
							'label' => 'Video Previews',
							'display' => 'block',
							'sub_fields' => array (
								array (
									'key' => $id . '_jGw081185Q2',
									'label' => 'Content',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_5T76275267f26',
									'label' => 'Video previews',
									'name' => 'video_previews',
									'type' => 'repeater',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'collapsed' => '',
									'min' => '',
									'max' => '',
									'layout' => 'block',
									'button_label' => 'Add video preview',
									'sub_fields' => array (
										array (
											'key' => $id . '_5T76277267f27',
											'label' => 'Content',
											'name' => 'content',
											'type' => 'wysiwyg',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'default_value' => '',
											'tabs' => 'all',
											'toolbar' => 'full',
											'media_upload' => 1,
										),
										array (
											'key' => $id . '_5T76278667f28',
											'label' => 'Video',
											'name' => 'video',
											'type' => 'oembed',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'width' => '',
											'height' => '',
										),
									),
								),
								array (
									'key' => $id . '_jGq2821RM315',
									'name' => 'tab2',
									'label' => 'Settings',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jGq0811RMa15',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa25',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							)
						),
						array(
							'key' => $id . '_q5a90Ab81z5',
							'name' => 'logos-gallery',
							'label' => 'Logos gallery',
							'display' => 'block',
							'sub_fields' => array(
								array(
									'key' => $id . '_q6a90Sa84z3',
									'name' => 'tab',
									'type' => 'tab',
									'label' => 'Content',
								),
								array (
									'key' => $id . '_q5a90sa84z3',
									'name' => 'logos-gallery',
									'label' => 'Logos gallery',
									'type' => 'gallery',
									'required' => '',
									'preview_size' => 'thumbnail',
									'insert' => 'append',
								),
								array(
									'key' => $id . '_q6q90Sa81z3',
									'name' => 'tab1',
									'type' => 'tab',
									'label' => 'Settings',
								),
								array (
									'key' => $id . '_jGq0811RMa16',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa26',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_q6Q20Sa81z3',
									'label' => 'Placement',
									'name' => 'placement',
									'type' => 'select',
									'required' => 0,
									'wrapper' => array (
										'width' => '25'
									),
									'choices' => array(
										'justify-content_start' => 'Left',
										'justify-content_center' => 'Center',
										'justify-content_end' => 'Right',
										'justify-content_sb' => 'Space between',
										'justify-content_sa' => 'Space around'
									),
									'default_values' => array( 'justify-content_start' )
								),
							)
						),
						array (
							'key' => $id . '_Q5m9Oib91z0',
							'name' => 'visualized-list',
							'label' => 'Visualized list',
							'display' => 'block',
							'sub_fields' => array (
								array(
									'key' => $id . '_Q5m9Oib82z1',
									'name' => 'tab',
									'type' => 'tab',
									'label' => 'Content',
								),
								array(
									'key' => $id . '_Q2m9oib73z1',
									'name' => 'title',
									'type' => 'text',
									'label' => 'Title',
									'placeholder' => 'Leave empty to use another title',
									'required' => ''
								),
								array (
									'key' => $id . '_Q5m9Oib83z2',
									'name' => 'visualized-list',
									'label' => 'Visualized list',
									'type' => 'repeater',
									'layout' => 'block',
									'sub_fields' => array (
										array (
											'key' => $id . '_Q5e9Oib74p9',
											'name' => 'icon',
											'label' => 'Icon',
											'type' => 'image',
											'required' => '',
											'wrapper' => array (
												'width' => 30
											),
											'return_format' => 'url',
											'conditional_logic' => array (
												array (
													array (
														'field' => $id . '_Q5m9Oib82p2',
														'operator' => '==',
														'value' => 'custom'
													)
												)
											),
										),
										array (
											'key' => $id . '_Q5m9Oib74z3',
											'name' => 'textarea',
											'label' => 'Textarea',
											'type' => 'text',
											'required' => '',
											'wrapper' => array (
												'width' => 70
											)
										),
										array (
											'key' => $id . '_Q5m9Oib85z4',
											'name' => 'sub-list',
											'label' => 'Sub list',
											'type' => 'repeater',
											'layout' => 'block',
											'sub_fields' => array (
												array (
													'key' => $id . '_Q5m9Oib66z5',
													'name' => 'textarea',
													'label' => 'Textarea',
													'type' => 'text',
													'required' => ''
												),
											),
											'conditional_logic' => array (
												array (
													array (
														'field' => $id . '_Q5m9Oib82p3',
														'operator' => '==',
														'value' => 'sublist'
													),
													array (
														'field' => $id . '_Q5m9Oib82p2',
														'operator' => '==',
														'value' => 'checklist'
													)
												)
											),
											'button_label' => 'Add sub-point'
										)
									),
									'collapsed' => $id . '_Q5m9Oib74z3',
									'button_label' => 'Add point'
								),
								array(
									'key' => $id . '_Q5m9Oib91p1',
									'name' => 'tab2',
									'type' => 'tab',
									'label' => 'Settings',
								),
								array (
									'key' => $id . '_jGq0811RMa17',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jGq0811RMa27',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_Q5m9Oib82p2',
									'name' => 'type',
									'label' => 'Type',
									'type' => 'select',
									'required' => '',
									'choices' => array (
										'checklist' => 'Checklist',
										'custom' => 'Custom icons'
									),
									'default_values' => array (
										'checklist'
									),
									'wrapper' => array (
										'width' => 33
									)
								),
								array (
									'key' => $id . '_Q5M9OiB81p2',
									'name' => 'offset',
									'label' => 'Offset',
									'type' => 'select',
									'required' => '',
									'choices' => array (
										'auto' => 'Auto',
										'none' => 'None'
									),
									'default_values' => array (
										'auto'
									),
									'wrapper' => array (
										'width' => 33
									)
								),
								array (
									'key' => $id . '_Q5m9Oib82p3',
									'name' => 'sublist',
									'label' => 'Sublist',
									'type' => 'select',
									'required' => '',
									'choices' => array (
										'none' => 'None',
										'sublist' => 'Use sublist'
									),
									'default_values' => array (
										'none'
									),
									'wrapper' => array (
										'width' => 33
									),
									'conditional_logic' => array(
										array (
											array (
												'field' => $id . '_Q5m9Oib82p2',
												'operator' => '==',
												'value' => 'checklist'
											)
										)
									)
								),
								array (
									'key' => $id . '_Q5m9Oib93p4',
									'name' => 'layout',
									'label' => 'Layout',
									'type' => 'select',
									'required' => '',
									'choices' => array (
										'auto' => 'Auto',
										'two-cols' => 'Two columns'
									),
									'default_values' => array (
										'auto'
									),
									'wrapper' => array (
										'width' => 33
									),
									'conditional_logic' => array(
										array (
											array (
												'field' => $id . '_Q5m9Oib82p2',
												'operator' => '==',
												'value' => 'checklist'
											)
										)
									)
								)
							)
						),
						array (
							'key' => $id . '_QSv081185Q2',
							'name' => 'image',
							'label' => 'Image',
							'display' => 'block',
							'sub_fields' => array(
								array (
									'key' => $id . '_jSv081g85Q2',
									'label' => 'Content',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_qSv081g85o2',
									'label' => 'Image',
									'name' => 'image',
									'type' => 'image',
									'required' => 0,
									'return_format' => 'object',
									'wrapper' => array(
										'width' => 70
									)
								),
								array (
									'key'           => $id . '_jSvWo12pMQ68',
									'label'         => 'Url',
									'name'          => 'url',
									'type'          => 'text',
									'required'      => 0,
									'placeholder'   => 'https://example.com/',
									'wrapper' => array(
										'width' => 30
									), 
									'conditional_logic' => array(
										array(
											array(
												'field'    => $id . '_jSvWo12pMzq77',
												'operator' => '==',
												'value'    => 'url'
											)
										)
									)
								), 
								array (
									'key'   => $id . '_jSv081104Q4',
									'label' => 'Page',
									'name'  => 'page',
									'type'  => 'page_link',
									'required' => 0,
									'ui' => 1,
									'conditional_logic' => array(
										array(
											array(
												'field'    => $id . '_jSvWo12pMzq77',
												'operator' => '==',
												'value'    => 'page'
											)
										)
									)
								),
								array (
									'key' => $id . '_jSv081185Q3',
									'label' => 'Settings',
									'name' => 'tab1',
									'type' => 'tab',
									'required' => ''
								),
								array (
									'key' => $id . '_jSv0811RMa11',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jSv0811RMa21',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_jSvWo12Rma04',
									'name' => 'align',
									'label' => 'Align',
									'type' => 'select',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'choices' => array (
										0 => 'Auto',
										'left' => 'Left',
										'center' => 'Center',
										'right' => 'Right'
									),
									'default_values' => array (
										0 => 'Auto'
									)
								),
								array (
									'key' => $id . '_jSvWo12Rma95',
									'name' => 'vertical',
									'label' => 'Vertical',
									'type' => 'select',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'choices' => array (
										0 => 'Auto',
										'top' => 'Top',
										'center' => 'Center',
										'bottom' => 'Bottom'
									),
									'default_values' => array (
										0 => 'Auto'
									)
								),
								array (
									'key' => $id . '_jSvWo12Rma86',
									'name' => 'max_width',
									'label' => 'Max Width',
									'type' => 'select',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'choices' => array (
										0 => '100%',
										'90%' => '90%',
										'80%' => '80%',
										'70%' => '70%',
										'60%' => '60%',
										'50%' => '50%',
										'40%' => '40%',
										'30%' => '30%',
										'20%' => '20%',
									),
									'default_values' => array (
										0 => '100%'
									)
								),
								array (
									'key'   => $id . '_jSvWo12pMzq77',
									'label' => 'Type',
									'name'  => 'type',
									'type'  => 'select',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'choices' => array (
										'simple' => 'Simple image',
										'url'    => 'URL',
										'page'   => 'Refer to another page',
										'popup'  => 'Open image inside a popup',
									),
									'default_values' => array (
										'image' => 'Simple image'
									)
								),
							),
							'min' => '',
							'max' => '',
						),
						array (
							'key' => $id . '_85m9Oib9123',
							'name' => 'accordion',
							'label' => 'Accordion',
							'display' => 'block',
							'sub_fields' => array (
								array(
									'key' => $id . '_85m9Oib9214',
									'name' => 'tab',
									'type' => 'tab',
									'label' => 'Content',
								),
								array (
									'key' => $id . '_85m9Oib9305',
									'name' => 'accordion-list',
									'label' => 'Accorion list',
									'type' => 'repeater',
									'layout' => 'block',
									'collapsed' => $id . '_85m9Oib9496',
									'sub_fields' => array (
										array (
											'key' => $id . '_85m9Oib9496',
											'label' => 'Visible headline',
											'name' => 'headline',
											'type' => 'text',
											'required' => '',
										),
										array (
											'key' => $id . '_85m9Oib9587',
											'label' => 'Hidden content area',
											'name' => 'content',
											'type' => 'wysiwyg',
											'required' => '',
										),
									),
									'button_label' => 'Add one more row'
								),
								array(
									'key' => $id . '_85m9Oib9678',
									'name' => 'tab2',
									'type' => 'tab',
									'label' => 'Settings',
								),
								array (
									'key' => $id . '_85m9Oib9769',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_85m9Oib9860',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							)
						),
						array (
							'key' => $id . '_i5g9Oib9214',
							'name' => 'google-table',
							'label' => 'Google Table',
							'display' => 'block',
							'sub_fields' => array (
								array(
									'key' => $id . '_i5g9Oib7214',
									'name' => 'tab',
									'type' => 'tab',
									'label' => 'Content',
								),
								array (
									'key' => $id . '_i5g9Oib9305',
									'name' => 'spreadsheetId',
									'label' => 'Table',
                  'type' => 'post_object',
                  'instructions' => 'Create a new Table and bind that with Google Spreadsheet',
                  'required' => 1,
                  'post_type' => array (
                    'table'
                  ),
                  'allow_null' => 0,
                  'multiple' => 0,
                  'return_format' => 'id',
                  'ui' => 1,
                  'wrapper' => array(
                    'width' => 50
                  ),
                ),
								array (
									'key' => $id . '_i5g1Oib9307',
									'name' => 'worksheetName',
									'label' => 'WorkSheet Name',
                  'type' => 'text',
                  'required' => 0,
                  'instructions' => 'Leave empty to get the first one from the spreadsheet',
                  'wrapper' => array(
                    'width' => 50
                  )
								),
								array(
									'key' => $id . '_i5g9Oib9678',
									'name' => 'tab2',
									'type' => 'tab',
									'label' => 'Settings',
								),
								array (
									'key' => $id . '_i5G9Oib9769',
									'name' => 'margin_top',
									'label' => 'Margin top',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
								array (
									'key' => $id . '_i5G9Oib9860',
									'name' => 'margin_bottom',
									'label' => 'Margin bottom',
									'type' => 'number',
									'required' => '',
									'wrapper' => array (
										'width' => 25
									),
									'append' => 'px'
								),
							)
						),
					)
				),
				array(
					'key' => $id . '_123o9Gcd8zX56',
					'name' => 'tab1',
					'label' => 'Settings',
					'type' => 'tab',
					'required' => ''
				),
				array (
					'key' => $id . '_123o9Gcd9zX54',
					'name' => 'width',
					'label' => 'Width',
					'type' => 'select',
					'required' => '',
					'wrapper' => array(
						'width' => '25'
					),
					'choices' => array(
						'auto' => 'Auto',
						'thin' => 'Thin'
					),
					'default_values' => array ( 'auto' )
				),
			)
		),

		// Options
		array (
			'key' => $id . '_5821e2e1f659b',
			'label' => 'Options',
			'name' => 'taboptions',
			'type' => 'tab',
			'required' => 0,
			'placement' => 'top',
		),
		array (
			'key' => $id . '_0014mn5CO4n1',
			'label' => 'Columns per line',
			'name' => 'cols-count',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33'
			),
			'placeholder' => 'Columns per line',
			'choices' => array(
				1 => 'Just one',
				2 => 'Two',
				3 => 'Three',
				4 => 'Four',
				5 => 'Five',
				6 => 'Six'
			),
			'default_values' => array( 0 => 1 )
		),
		array (
			'key' => $id . '_5821e6f3d49fc',
			'label' => 'Layout',
			'name' => 'layout',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33',
			),
			'choices' => array (
				'grid' => 'Grid',
				'carousel' => 'Carousel',
			),
			'default_value' => array (
				0 => 'grid',
			)
		),
		array (
			'key' => $id . '_5821e2fcf659c',
			'label' => 'Columns\' margins',
			'name' => 'cols-margins',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33',
			),
			'choices' => array (
				'none' => 'None',
				'normal' => 'Normal',
			),
			'default_value' => array (
				0 => 'none',
			)
		),						
		array (
			'key' => $id . '_5821e359f659d',
			'label' => 'Width',
			'name' => 'width',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33',
			),
			'choices' => array (
				'default' => 'Default',
				'full' => 'Full',
				'four-five' => '4/5',
				'three-four' => '3/4',
				'one-two' => '1/2',
			),
			'default_value' => 'auto',
		),
		array (
			'key' => $id . '_5821eelementali34',
			'label' => 'Position',
			'name' => 'position',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33'
			),
			'choices' => array (
				'left' => 'Left',
				'center' => 'Center',
				'right' => 'Right',
			),
			'default_value' => array (
				0 => 'center',
			)
		),
		array (
			'key' => $id . '_5c21c44ff659c',
			'label' => 'Columns placement',
			'name' => 'cols-placement',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array (
				'width' => '33'
			),
			'choices' => array (
				'stretch' => 'Stretch',
				'top' => 'Top',
				'middle' => 'Middle',
				'bottom' => 'Bottom',
			),
			'default_value' => array( 'middle' )
		),								
		array (
			'key' => $id . '_18212372d49ff',
			'label' => 'Margin top',
			'name' => 'margin_top',
			'type' => 'number',
			'required' => 0,
			'default_value' => 0,
			'min' => 0,
			'max' => 200,
			'step' => 10,
			'append' => 'px',
			'wrapper' => array(
				'width' => 33
			),
		),						
		array (
			'key' => $id . '_1821291129ff',
			'label' => 'Margin bottom',
			'name' => 'margin_bottom',
			'type' => 'number',
			'required' => 0,
			'default_value' => 0,
			'min' => 0,
			'max' => 200,
			'step' => 10,
			'append' => 'px',
			'wrapper' => array(
				'width' => 33
			),
		),

		// Grid settings
		array (
			'key' => $id . '_5821E2e1f659b',
			'label' => 'Grid settings',
			'name' => 'tabopt1ons',
			'type' => 'tab',
			'required' => 0,
			'placement' => 'top',
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'grid'
					)
				),
			),
		),
		array (
			'key' => $id . '_cePuha1234',
			'label' => 'Columns align',
			'name' => 'cols-align',
			'type' => 'select',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'grid'
					)
				),
			),
			'wrapper' => array (
				'width' => '33'
			),
			'choices' => array (
				'left' => 'Left',
				'center' => 'Center',
				'right' => 'Right',
				'space-around' => 'Space Around',
				'space-between' => 'Space Between'
			),
			'default_value' => array (
				0 => 'left',
			)
		),
		array (
			'key' => $id . '_co1231era9ti5od',
			'label' => 'The ratio of rows',
			'name' => 'cols-ratio',
			'type' => 'select',
			'required' => 0,
			'wrapper' => array(
				'width' => 33
			),
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_0014mn5CO4n1',
						'operator' => '==',
						'value' => '2',
					),
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'grid'
					)
				),
			),
			'choices' => array (
				'equal' => '50% : 50%',
				'33-66' => '33% : 66%',
				'40-60' => '40% : 60%',
				'45-55' => '45% : 55%',
				'66-33' => '66% : 33%',
				'55-45' => '55% : 45%',
				'60-40' => '60% : 40%',
			),
			'default_value' => 'equal',
		),

		// Carousel settings
		array (
			'key' => $id . '_5821Q2e1f659b',
			'label' => 'Carousel settings',
			'name' => 'tabopt2Cans',
			'type' => 'tab',
			'required' => 0,
			'placement' => 'top',
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'carousel'
					)
				),
			),
		),								
		array (
			'key' => $id . '_1822e772d49ff',
			'label' => 'How many slides to scroll',
			'name' => 'slides_scroll',
			'type' => 'number',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'carousel'
					)
				),
			),
			'default_value' => 1,
			'min' => 1,
			'step' => 1,
			'wrapper' => array(
				'width' => 33
			),
		),
		array (
			'key' => $id . '_arr28oo659a',
			'label' => 'Show dots?',
			'name' => 'show_dots',
			'type' => 'select',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => $id . '_5821e6f3d49fc',
						'operator' => '==',
						'value' => 'carousel'
					)
				),
			),
			'wrapper' => array (
				'width' => '33',
			),
			'choices' => array (
				'false' => 'None',
				'true' => 'Yes, show'
			),
			'default_value' => array (
				0 => 'false',
			),
		),
	);
}

/**
 * Generates Section local fields array
 */
function get_section_local_field( $id = '1234QWERasdf' ) {
	return array (
		array (
			'key' => $id . '_5965004cd9951',
			'label' => 'Rows',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $id . '_59650072d9952',
			'label' => 'List of rows',
			'name' => 'rows',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Row',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => $id . 65008008512,
					'name' => 'row',
					'label' => 'Row',
					'display' => 'block',
					'sub_fields' => get_row_local_field( $id ),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => $id . '13050003e333f',
					'name' => 'row-template',
					'label' => 'Template',
					'display' => 'block',
					'sub_fields' => array(
						array (
							'key' => $id . '12650003e333f',
							'label' => 'Choose row template',
							'name' => 'row-template',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'post_type' => array (
								'template'
							),
							'allow_null' => 0,
							'multiple' => 0,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
			),
		),
		array (
			'key' => $id . '_59650094d9953',
			'label' => 'View',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $id . '_59650133d9956',
			'label' => 'Paddigns',
			'name' => 'vertical_paddigns',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 15,
			),
			'choices' => array (
				0 => 'None',
				'small' => 'Small',
				'middle' => 'Middle',
				'large' => 'Large',
			),
			'default_value' => array (
				0 => 'middle',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => $id . '_5965009fd9954',
			'label' => 'Section color',
			'name' => 'color',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 35,
			),
			'choices' => array (
				'light' => 'White bg and dark text',
				'dark' => 'Dark bg and light text',
			),
			'default_value' => array (
				0 => 'light',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => $id . '_596500cdd9955',
			'label' => 'Background image',
			'name' => 'background_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => $id . '_596502de4dd35',
			'label' => 'Options',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => '',
			'conditional_logic' => '',
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $id . '_596502e94dd36',
			'label' => 'Section ID',
			'name' => 'section_id',
			'type' => 'text',
			'instructions' => '',
			'required' => '',
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'value',
			'prepend' => '#',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => $id . '_596502e95sa27',
			'label' => 'CSS Snippets',
			'name' => 'css-snippets',
			'type' => 'textarea',
			'placeholder' => '/* Write your css code here.  */',
			'required' => '',
			'new_lines' => ''
		),
	);
}