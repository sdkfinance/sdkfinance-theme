<?php 
/**
 * Includes all local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

include get_template_directory() . '/inc/local-fields/api.php';
include get_template_directory() . '/inc/local-fields/primary-content.php';
include get_template_directory() . '/inc/local-fields/section-template.php';
include get_template_directory() . '/inc/local-fields/row-template.php';
include get_template_directory() . '/inc/local-fields/header.php';
include get_template_directory() . '/inc/local-fields/single.php';
include get_template_directory() . '/inc/local-fields/whitepaper.php';
?>
