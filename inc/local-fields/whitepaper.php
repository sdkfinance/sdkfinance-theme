<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


acf_add_local_field_group(array (
	'key' => 'group_59s4xfx5d8fd2',
	'title' => 'Sidebar',
	'fields' => array (
    array (
      'key' => 'wp_z964dfd8d9950',
      'label' => 'Widgets',
      'name' => 'widgets',
      'type' => 'flexible_content',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'button_label' => 'Add widget',
      'min' => '',
      'max' => '1',
      'layouts' => array (
        array (
          'key' => 'wp_Q50080293W8',
          'label' => 'Form',
          'name' => 'form',
          'display' => 'block',
          'sub_fields' => get_form_local_field('wp'),
          'min' => '',
          'max' => '',
        ),
      )
    )
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'tpl-whitepaper.php',
			),
		),
	),
	'menu_order' => 0,
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));