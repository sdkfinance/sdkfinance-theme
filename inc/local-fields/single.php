<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5164fd8999f12',
	'title' => 'Accordion above related articles section',
	'fields' => array (
    array (
      'key' => 'sngl_85m9Oib9305',
      'name' => 'accordion-list',
      'label' => 'Accorion list',
      'type' => 'repeater',
      'layout' => 'block',
      'collapsed' => 'sngl_85m9Oib9496',
      'sub_fields' => array (
        array (
          'key' => 'sngl_85m9Oib9496',
          'label' => 'Visible headline',
          'name' => 'headline',
          'type' => 'text',
          'required' => '',
        ),
        array (
          'key' => 'sngl_85m9Oib9587',
          'label' => 'Hidden content area',
          'name' => 'content',
          'type' => 'wysiwyg',
          'required' => '',
        ),
      ),
      'button_label' => 'Add one more row'
    ),
  ),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>