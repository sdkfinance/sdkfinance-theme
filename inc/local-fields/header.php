<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'head_5964ff8999fd7',
	'title' => 'Header options',
	'fields' => array (
		array (
			'key'      => 'head_x96cow8v9bs02',
			'label'    => 'Header and footer type',
			'name'     => 'header-footer-type',
			'type'     => 'select',
			'required' => '',
			'choices'  => array(
				'mini' => 'Minimized'
			),
			'wrapper' => array (
				'width' => 33
			),
			'allow_null' => 1
		),
		array (
			'key'      => 'head_5964ow8999305',
			'label'    => 'Show?',
			'name'     => 'show-heroarea',
			'type'     => 'select',
			'required' => '',
			'choices'  => array(
				'hide' => 'Hide'
			),
			'default_values' => array ( 'show' ),
			'wrapper' => array (
				'width' => 33
			),
			'allow_null' => 1
		),
		array (
			'key'      => 'head_5964ow9808496',
			'label'    => 'Image',
			'name'     => 'bg-heroarea',
			'type'     => 'image',
			'required' => '',
			'return_format' => 'url',
			'preview_size' => 'full',
			'wrapper' => array (
				'width' => 67
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'business-cases',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'use-cases',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>