<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


if( function_exists('acf_add_local_field_group') ):


acf_add_local_field_group(array (
	'key' => 'group_5964ff8999fd7',
	'title' => 'Primary content',
	'fields' => array (
		array (
			'key' => 'field_5964ffd8d9950',
			'label' => 'Primary content',
			'name' => 'primary_content',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Section',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => '59650003e333f',
					'name' => 'section',
					'label' => 'Section',
					'display' => 'block',
					'sub_fields' => get_section_local_field( 'prim-cont' ),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '49650003e333f',
					'name' => 'section-template',
					'label' => 'Template',
					'display' => 'block',
					'sub_fields' => array(
						array (
							'key' => '48650003e333f',
							'label' => 'Choose section template',
							'name' => 'section-template',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'post_type' => array (
								'template'
							),
							'allow_null' => 0,
							'multiple' => 0,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '5965o002w333f',
					'name' => 'page-navigation',
					'label' => 'Page navigation',
					'display' => 'block',
					'max' => '1',
					'sub_fields' => array (
						array(
							'key' => 'page-navi_abcd10',
							'name' => 'page-navigation',
							'label' => 'Page navigation',
							'type' => 'repeater',
							'required' => '',
							'sub_fields' => array (
								array (
									'key' => 'page-navi_abcd29',
									'name' => 'label',
									'label' => 'Label',
									'type' => 'text',
									'required' => '',
								),
								array (
									'key' => 'page-navi_abcd38',
									'name' => 'id',
									'label' => 'ID of target section',
									'type' => 'text',
									'required' => '',
									'prepend' => '#',
									'placeholder' => 'don-t_use_spaces-or-symbols'
								)
							)
						)
					)
				),
				array (
					'key' => '5967321y1242a',
					'name' => 'use-cases',
					'label' => 'Use cases',
					'display' => 'block',
					'sub_fields' => array(),
					'min' => '',
					'max' => '1',
				),
			),
		),
	),
	'location' => array (
		array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
      array (
        'param' => 'page_template',
        'operator' => '!=',
        'value' => 'tpl-whitepaper.php',
      ),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'business-cases',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'use-cases',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_5964ff8568fd9',
	'title' => 'Special information',
	'fields' => array (
		array(
			'key' => 'us_48650003e333f',
			'label' => 'Schelme',
			'name' => 'scheme-img',
			'type' => 'image',
			'required' => 0,
			'return_format' => 'url',
			'preview_size' => 'full',
			'wrapper' => array(
				'width' => 50
			)
		),
		array(
			'key' => 'us_48650003e124f',
			'label' => 'Description',
			'name' => 'scheme-desc',
			'type' => 'wysiwyg',
			'required' => 0,
			'wrapper' => array(
				'width' => 50
			)
		)
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'use-cases',
			),
		),
	),
	'menu_order' => 0,
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>