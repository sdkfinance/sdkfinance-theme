<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


acf_add_local_field_group(array (
	'key' => 'group_596506937549f',
	'title' => 'Section template',
	'fields' => array (
		array (
			'key' => 'sel_5964ffd8d9950',
			'label' => 'Primary content',
			'name' => 'primary-content-template',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Section',
			'min' => '',
			'max' => '1',
			'layouts' => array (
				array (
					'key' => 'sel_59650003e333f',
					'name' => 'section',
					'label' => 'Section',
					'display' => 'block',
					'sub_fields' => get_section_local_field( 'se-te' ),
				)
			)
		)
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'template',
			),
			array (
				'param' => 'post_taxonomy',
				'operator' => '==',
				'value' => 'template-type:section',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

?>