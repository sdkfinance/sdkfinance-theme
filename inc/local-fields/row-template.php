<?php 
/**
 * ACF local fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

acf_add_local_field_group(array (
	'key' => 'group_596506b1c770e',
	'title' => 'Row template',
	'fields' =>  array (
		array (
			'key' => 'row_5964ffd8d9950',
			'label' => 'Rows',
			'name' => 'row-template',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'button_label' => 'Add Row',
			'min' => '',
			'max' => '1',
			'layouts' => array (
				array (
					'key' => 'row_59650003e333f',
					'name' => 'row',
					'label' => 'Row',
					'display' => 'block',
					'sub_fields' => get_row_local_field( 'ro-te' )
				)
			)
		)
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'template',
			),
			array (
				'param' => 'post_taxonomy',
				'operator' => '==',
				'value' => 'template-type:row',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

?>