<?php 
/**
 * Use cases post type
 *
 * Change User cases to post type name and _EXAMPLESLUG_ to Slugname
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

add_action('init', 'init_use_cases_post_type');

function init_use_cases_post_type(){

    register_post_type('use-cases', array(
        'labels'                 => array(
            'name'               => 'Use cases', 
            'singular_name'      => 'Use case', 
            'add_new'            => 'Add new',
            'add_new_item'       => 'Add new Use case',
            'edit_item'          => 'Edit Use case',
            'new_item'           => 'New Use case',
            'view_item'          => 'View Use case',
            'search_items'       => 'Find Use case',
            'not_found'          => 'There are not any Use cases',
            'not_found_in_trash' => 'There are not any Use cases in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Use cases'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'menu_icon'          => 'dashicons-groups',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 6,
        'supports'           => array('title','editor','author','thumbnail')
    ) );

}

?>