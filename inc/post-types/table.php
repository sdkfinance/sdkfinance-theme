<?php 
/**
 * Table post type
 *
 * Change Template to post type name and template to Slugname
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

add_action('init', 'init_table_post_type');

function init_table_post_type(){

    register_post_type('table', array(
        'labels'                 => array(
            'name'               => 'Tables',
            'singular_name'      => 'Table',
            'add_new'            => 'Add new',
            'add_new_item'       => 'Add new Table',
            'edit_item'          => 'Edit Table',
            'new_item'           => 'New Table',
            'view_item'          => 'View Table',
            'search_items'       => 'Find Table',
            'not_found'          => 'There are not any Table',
            'not_found_in_trash' => 'There are not any Table in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Tables'

        ),
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array('title'),
        'menu_icon'          => 'dashicons-media-spreadsheet'
    ) );

}

?>