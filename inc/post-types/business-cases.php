<?php 
/**
 * Example post type
 *
 * Change Business cases to post type name and business-cases to Slugname
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

add_action('init', 'init_business_cases_post_type');

function init_business_cases_post_type(){

    register_post_type('business-cases', array(
        'labels'                 => array(
            'name'               => 'Business cases', 
            'singular_name'      => 'Business case', 
            'add_new'            => 'Add new',
            'add_new_item'       => 'Add new Business case',
            'edit_item'          => 'Edit Business case',
            'new_item'           => 'New Business case',
            'view_item'          => 'View Business case',
            'search_items'       => 'Find Business case',
            'not_found'          => 'There are not any Business cases',
            'not_found_in_trash' => 'There are not any Business cases in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Business cases'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'page',
        'menu_icon'          => 'dashicons-admin-site',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 6,
        'supports'           => array('title','editor','author','thumbnail')
    ) );

}

?>