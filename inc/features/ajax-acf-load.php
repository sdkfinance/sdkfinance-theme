<?php 
/**
 * AJAX Load ACF fields
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


// TODO: TEST


add_action('wp_ajax_ajax_acf_load', 'ajax_acf_load');
add_action('wp_ajax_nopriv_ajax_acf_load', 'ajax_acf_load');

function ajax_acf_load() {
	
	/**
	 * Verify current user
	 */
	if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'ajax_nonce' ) ) :
		echo json_encode( array( 'content' => 'Nonce aint valid. Posted nonce is: ' + $_POST['nonce'], 'more' => false, 'offset' => 1, 'console' => true ) );
		exit;
	endif;
	
	/**
	 * If post ID or offset was missed, return promisse 
	 */
	if (!isset($_POST['post_id']) || !isset($_POST['offset'])) :
		echo json_encode( array( 'content' => 'Post id or offset aint defined', 'more' => false, 'offset' => 1, 'console' => true ) );
		exit;
	endif;

	/**
	 * Common variables
	 */
    define( 'GET_SECTION_AJAX', true );
    $start   = $_POST['status'];
    $post_id = $_POST['post_id'];
    $field   = $_POST['field'];
    $offset  = $_POST['offset'];
    $more    = false;
	$show    = 1; 
	$end     = $start + $show;

	ob_start();

    if ( have_rows( $field, $post_id ) ) :
        $total          = count( get_field( $field, $post_id ) );
        $count          = 1;
        $section_number = 0;
        $times          = 0;

        while ( have_rows( $field, $post_id ) ) :
            $the_section_row = the_row();
            $section_id      = 'pr-co';
            $section_number++;

            if ( $count > $start ) :
                include get_template_directory() . '/template-parts/' . get_row_layout() . '.php';
                $times++;

                if ( $times >= $offset )
                    break;
            endif;

            $count++; 
        endwhile;

    else :
		echo json_encode( array( 'content' => "{$field} field with id {$post_id} -- aint exist", 'more' => false, 'offset' => 1, 'console' => true ) );
		exit;
    endif;

	$content = ob_get_clean();

	$more = false;
	if ($total > $count) 
		$more = true;

	echo json_encode( array( 'content' => $content, 'more' => $more, 'status' => $count, 'console' => false ) );
	exit;

}


?>