<?php 
/**
 * Navigations
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

register_nav_menus( array(
	'primary'  => esc_html__( 'Primary' ),
	'action'   => esc_html__( 'Actions in Header' ),
	'footer-1' => esc_html__( 'Footer Sidebar 1' ),
	'footer-2' => esc_html__( 'Footer Sidebar 2' ),
	'footer-3' => esc_html__( 'Footer Sidebar 3' ),
	'footer-4' => esc_html__( 'Footer Sidebar 4' ),
	'footer-5' => esc_html__( 'Footer Sidebar 5' ),
) );

?>