<?php  
/**
 * Shortcodes
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

/**
 * Button shourtcode
 */
add_shortcode( 'template', 'shortcode_template' );
function shortcode_template( $atts ) {
    $atts = shortcode_atts( array(
      'id'    => ''  
    ), $atts );

    if ( ! $atts['id'] )
        return 'error';

    // override $post
    // $post = $atts['id'];
    // setup_postdata( $post );

    ob_start();
    /**
     * Check whether Section template post has a sections.
     * Pull that if it exists
     */
    if ( have_rows( 'primary-content-template', $atts['id'] ) ) :
        while ( have_rows( 'primary-content-template', $atts['id'] ) ) :
            $the_section_row = the_row();
            $section_id      = 'sec-tem';

            include get_template_directory() . '/template-parts/section.php';
        endwhile;
    endif;
    wp_reset_postdata();

    $content = ob_get_clean();

    return $content;
}

/**
 * Button shourtcode
 */
add_shortcode( 'btn', 'shortcode_button' );
function shortcode_button( $attrs ) {
    $attrs = shortcode_atts( array(
      'label'    => '',
      'url'      => 'javascript:' ,
      'rel'      => '',
      'download' => '',
      'target'   => ''
    ), $attrs );

    $attributes   = array();
    $attributes[] = "href='{$attrs['url']}'";
    $attributes[] = 'class="button"';
    $attributes[] = $attrs['rel'] ? "rel='{$attrs['rel']}'" : '';
    $attributes[] = $attrs['download'] ? "download" : '';
    $attributes[] = $attrs['target'] ? "target='_blank'" : '';
    $attributes   = generate_classlist( $attributes );

    return "<a {$attributes}>{$attrs['label']}</a>";
}

/**
 * Hours table shortcode
 */
add_shortcode( 'hours-table', 'shortcode_hours_table' );
function shortcode_hours_table( $atts ) {
    $atts = shortcode_atts( array(
      'title'   => '',
      'label'    => ''  
    ), $atts );

    return "<div class='display_flex justify-content_sb margin-top_5px'><div>{$atts['title']}</div><nobr class='is-brand-back-medium font_bold'>{$atts['label']}</nobr></div>";
}

/**
 * Table shortcode
 */
add_shortcode( 'google-table', 'shortcode_google_table' );
function shortcode_google_table( $atts ) {
    $atts = shortcode_atts( array(
      'id' => '0',
      'name' => ''
    ), $atts );

    return "<div data-table-id='{$atts['id']}' data-table-name='{$atts['name']}'>Loading...</div>";
}

/**
 * Button shourtcode
 */
add_shortcode( 'color', 'shortcode_color' );
function shortcode_color( $atts ) {
    $atts = shortcode_atts( array(
      'text'   => '',
      'color'  => '',
      'class'  => ''  
    ), $atts );

    switch ($atts['color']) :
    	case 'Dark':
    		$atts['class'] = 'is-dark';
    		break; 

    	case 'Light':
    		$atts['class'] = 'is-light';
    		break;

    	case 'Gray':
    		$atts['class'] = 'is-gray';
    		break;

    	case 'Gradient':
    		$atts['class'] = 'is-brand-gradient';
    		break;

    	case 'Base':
    		$atts['class'] = 'is-brand-base';
    		break;

    	case 'Light Base':
    		$atts['class'] = 'is-brand-base-light';
    		break;

    	case 'Back color':
    		$atts['class'] = 'is-brand-back';
    		break;
    		
    	case 'Back light':
    		$atts['class'] = 'is-brand-back-light';
    		break;
    		
    	case 'Back medium':
    		$atts['class'] = 'is-brand-back-medium';
    		break;
    endswitch;

   return "<span><div class='{$atts['class']}'>{$atts['text']}</div></span>";
}

?>