<?php  
/**
 * Common functions
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


/**
 * Helper function
 */
function print_r_html( $value ) {
    echo '<pre>';
    print_r( $value );
    echo '</pre>';
}

add_filter('excerpt_more', 'excerpt_more_to_link');
function excerpt_more_to_link($more) {
    return '...';
}


/**
 * Get metadata of the post
 */
function get_post_metadata() {
    global $post;
    global $authordata;

    $author_url  = get_author_posts_url($authordata->ID);
    $meta        = 'Posted on <nobr>'.get_the_date( 'F j' ).',</nobr> '.get_the_date( 'Y' ).' by <nobr><a href="'.$author_url.'">'.get_the_author().'</a></nobr>';
    return $meta;
}

/**
 * Create style prefix.
 * Convert number to string 
 * 
 * @param  string $i 
 * @return string    
 */
function create_style_prefix( $i = '' ) {
    switch ( $i ) :
        
        case 1:
            $prefix = 'one';
            break;
        
        case 2:
            $prefix = 'two';
            break;
        
        case 3:
            $prefix = 'three';
            break;
        
        case 4:
            $prefix = 'four';
            break;
        
        case 5:
            $prefix = 'five';
            break;
        
        case 6:
            $prefix = 'six';
            break;
        
        case 7:
            $prefix = 'seven';
            break;
        
        case 8:
            $prefix = 'eight';
            break;
        
        case 9:
            $prefix = 'nine';
            break;
        
        case 10:
            $prefix = 'ten';
            break;
        
        default:
            $prefix = 'zero';
            break;

    endswitch;

    return $prefix;
}


/**
 * Transform name for using in strings/slugs/urls
 * 
 * @param  string $name  - handle string
 * @param  string $itype - on which symbol will be chenged ' ' 
 * @return string
 */
function transform_name( $name = '', $type = '' ) {
    $word = array( ' ' => $type, '&' => '' );
    $new = strtr( $name, $word );
    $new = strtolower( $new );

    return $new;
}

/**
 * Adapt image
 *
 * - Return an image in the more better size.
 * - Make circle if it's required.
 * - Wrap into <a href=""> if $link is existed
 * 
 * @param  integer $id     
 * @param  integer $width  
 * @param  integer $height 
 * @param  boolean $link   optional
 * @param  [type]  $attr   
 * @param  boolean $circle 
 * @return null         
 */
function adapt_image( $id=0, $width=0, $height=0, $link=false, $attr=null, $circle=false, $blank=false ) {

    $html = '';

    if ( $circle !== false ) :
        if ( $width > $height ) : 
            $width = $height;
        elseif ( $width <= $height ) : 
            $height = $width;
        endif;

        $html .= '<div class="is-circle">';
    endif;

    $link_attrs  = $link ? "href='{$link}'" : "href='#.'";
    $link_attrs .= $blank ? " target='_blank'" : '';

    $html .= $link ? "<a {$link_attrs}>" : '';
    $html .= wp_get_attachment_image( $id, array( $width, $height ), true, $attr );
    $html .= $link ? '</a>' : '';

    if ( $circle !== false )
        $html .= '</div>'; 

}


/**
 * Column borders
 *
 * - Add borders to column
 * 
 * @param  array  $border 
 * @return string
 */
function print_content_card_border( $border = array() ) {

    $html = '';
    $attr = '';

    if ( $border['is'] == 'pc--c__border-left' ) :

        $attr .= "border-left: {$border['width']} {$border['style']} {$border['color']};";
        $attr .= "left: 0;";
        $attr .= "height: {$border['size']};";

        $html .="<div class='pc--c__border pc--c__border-side_left' style='{$attr}'></div>";

    elseif ( $border['is'] == 'pc--c__border-right' ) :

        $attr .= "border-right: {$border['width']} {$border['style']} {$border['color']};";
        $attr .= "right: 0;";
        $attr .= "height: {$border['size']};";

        $html .="<div class='pc--c__border pc--c__border-side_right' style='{$attr}'></div>";

    elseif ( $border['is'] == 'pc--c__border-both' ) :

        $attr .= "border-right: {$border['width']} {$border['style']} {$border['color']};";
        $attr .= "height: {$border['size']};";

        $html .="<div class='pc--c__border pc--c__border-side_left' style='{$attr} left: 0;'></div>";
        $html .="<div class='pc--c__border pc--c__border-side_right' style='{$attr} right: 0;'></div>";

    endif;

    return $html;
}


/**
 * Just generate random string.
 * Uses numbers and latin words.
 * 
 * @param number $length - count of symbols
 * 
 * @return string
 */
function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < $length; $i++) :
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    endfor;

    return $randomString;
} 


/**
 * Insert reCaptch html
 *
 * 1. Generate HTML if key is exist
 * 2. Enqueue recaptcha script
 * 
 * @return string
 */
function insert_recaptcha_html() {

    $html = get_field('re_captcha', 'apikey') ? '<div class="g-recaptcha" data-sitekey="'.get_field('re_captcha', 'apikey').'"></div>' : '';

    return $html;
}


/**
 * Crop content
 */

function crop_content( $charlength = 50, $text = '', $end = '[...]' ) {
    $charlength++;
    $string = $text;

    if ( mb_strlen( $text ) > $charlength ) :
        $subex = mb_substr( $text, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

        if ( $excut < 0 ) :
            $string = mb_substr( $subex, 0, $excut );
        else :
            $string = $subex;
        endif;

        $string .= $end;
    endif;

    return $string;
}

/**
 * Generate classlist from array
 */
function generate_classlist( $array ) {
    if ( !is_array( $array ) || empty( $array ) ) 
        return '';

    // That will be returned
    $classlist = '';
    $counter   = 0;

    foreach ( $array as $class ) :
        if ( $class != '' ) : 
            if ( $counter !== 0 )  
                $classlist .= ' ';

            $classlist .= "{$class}";
        endif;
        
        $counter++;
    endforeach;

    return $classlist;
}


/**
 * Generate video background html
 * 
 * @param  string $webm   
 * @param  string $ogv    
 * @param  string $mp4    
 * @param  array  $poster [0] - url 
 * @param  bool   echo
 * @param  bool   autolay attr
 * @param  bool   loop attr
 * 
 * @return string
 */
function generate_video_bg_html( $webm = '', $ogv = '', $mp4 = '', $poster = '', $echo = false, $autoplay = true, $loop = true ) {
    $attr_auto   = $autoplay ? 'autoplay' : '';
    $attr_loop   = $loop ? 'loop' : '';
    $attr_poster = $poster[0] ? "poster='{$poster[0]}'" : '';
    $source_webm = $webm ? "<source src='{$webm}' type='video/mp4' />" : '';
    $source_ogv  = $ogv ? "<source src='{$ogv}' type='video/webm' />" : '';
    $source_mp4  = $mp4 ? "<source src='{$mp4}' type='video/ogg' />" : '';
    $img_poster  = $poster[0] ? "<img src='{$poster[0]}' alt='' />" : '';
    $is_sources  = ($source_webm || $source_ogv || $source_mp4 || $poster);

    /* If there is anything that we can show, go on */
    $output = $is_sources ? "
        <video class='background-video' {$attr_poster} {$attr_auto} {$attr_loop}>
            {$source_webm}
            {$source_ogv}
            {$source_mp4}
            {$img_poster}
        </video>       
    " : '';

    if ($echo) :
        echo $output;
    else : 
        return $output;
    endif;
}

/**
 * Function to get the client IP address
 */
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_X_REAL_IP']))
        $ipaddress = $_SERVER['HTTP_X_REAL_IP'];
    else if (isset($_SERVER['HTTP_CLIENT_IP']))
       $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
       $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
       $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
       $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
       $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

/**
 * log_filled_form()
 */
function log_filled_form() {
    /**
     * Save spreadsheet
     */

    // that is a data model on spreadsheet side
    $data = array(
        0 => date("F d, Y h:i:s A"), // date
        1 => $_POST['page'], // page
        2 => $_POST['email'], // email
        3 => get_client_ip() == '::1' ? 'Dev envinroment' : do_shortcode('[geoip_detect2 property="country"]'),
    );

    $PostAContact = '';

    include_once get_template_directory() . '/plugins/phpgooglespreadsheetapi/postacontact.php';

    $output = array(
        'response'   => is_object($PostAContact) ? $PostAContact->execute($data) : ['status' => 500],
        'redirect' => true,
        'data'     => $data
    );

    echo json_encode( $output );

    exit;
}

add_action( 'wp_ajax_log_filled_form', 'log_filled_form' );
add_action( 'wp_ajax_nopriv_log_filled_form', 'log_filled_form' );

/**
 * log_filled_form_paypal()
 */
function log_filled_form_paypal() {
    /**
     * Save spreadsheet
     */

    $data = array(
        0 => date("F d, Y h:i:s A"), // date
        1 => $_POST['page'], // page
        2 => $_POST['email'], // email
        3 => $_POST['brand'], // brand
        4 => $_POST['logotype'], // logotype
        5 => 'Redirected to PayPal', // status
        6 => get_client_ip() == '::1' ? 'Dev envinroment' : do_shortcode('[geoip_detect2 property="country"]'),
        7 => '---',
    );

    $PostAContactPayPal = '';

    include_once get_template_directory() . '/plugins/phpgooglespreadsheetapi/postacontact-paypal.php';

    try {
      $output = array(
        'status'   => is_object($PostAContactPayPal) ? $PostAContactPayPal->execute($data) : ['status' => '500'],
        'redirect' => false,
        'data'     => $data,
      );
    } catch (Exception $e) {
      $output = array(
        'response' => ['status' => '500'],
        'redirect' => false,
        'data'     => $data,
        'error'    => $e->getMessage()
      );
    }

    echo json_encode( $output );

    exit;
}

add_action( 'wp_ajax_log_filled_form_paypal', 'log_filled_form_paypal' );
add_action( 'wp_ajax_nopriv_log_filled_form_paypal', 'log_filled_form_paypal' );

/**
 * log_filled_form_paypal()
 */
function log_filled_form_whitepaper() {
    /**
     * Save spreadsheet
     */

    $data = array(
        0 => date("F d, Y h:i:s A"), // date
        1 => $_POST['page'], // page
        2 => $_POST['first'], // first name
        3 => $_POST['last'], // last name
        4 => $_POST['company'], // company name
        5 => $_POST['role'], // job title
        6 => $_POST['phone'], // phone
        7 => $_POST['email'], // email
        8 => get_client_ip() == '::1' ? 'Dev envinroment' : do_shortcode('[geoip_detect2 property="country"]'),
    );

    $PostAContactWhitePaper = '';

    include_once get_template_directory() . '/plugins/phpgooglespreadsheetapi/postacontact-whitepaper.php';

    try {
      $output = array(
        'status'   => is_object($PostAContactWhitePaper) ? $PostAContactWhitePaper->execute($data) : ['status' => '500'],
        'redirect' => true,
        'data'     => $data,
      );
    } catch (Exception $e) {
      $output = array(
        'response' => ['status' => '500'],
        'redirect' => true,
        'data'     => $data,
        'error'    => $e->getMessage()
      );
    }

    echo json_encode( $output );

    exit;
}

add_action( 'wp_ajax_log_filled_form_whitepaper', 'log_filled_form_whitepaper' );
add_action( 'wp_ajax_nopriv_log_filled_form_whitepaper', 'log_filled_form_whitepaper' );

/**
 * upload_file_from_form()
 */
function upload_file_from_form() {
    $output = array();

    if (!function_exists('wp_handle_upload')) {
      require_once(ABSPATH . 'wp-admin/includes/file.php');
    }

    $uploadedfile     = $_FILES['logotype'];
    $upload_overrides = array('test_form' => false);
    $movefile         = wp_handle_upload($uploadedfile, $upload_overrides);

    if ($movefile && !isset($movefile['error'])) {
      $output['status'] = $movefile;
    } else {
      /**
       * Error generated by _wp_handle_upload()
       * @see _wp_handle_upload() in wp-admin/includes/file.php
       */
      $output['status'] = $movefile['error'];
    }

    echo json_encode( $output );

    exit;
}

add_action( 'wp_ajax_upload_file_from_form', 'upload_file_from_form' );
add_action( 'wp_ajax_nopriv_upload_file_from_form', 'upload_file_from_form' );

/**
 * Table getter
 */
function init_table_getter() {

    /**
     * Save spreadsheet
     */

    $params = array(
      'spreadsheetKey'  => isset( $_POST['spreadsheetId'] ) ? get_field( 'table-key', $_POST['spreadsheetId']) : '',
      'worksheetName'   => isset( $_POST['worksheetName'] ) ? $_POST['worksheetName'] : ''
    );

    $TableGetter = '';

    include_once get_template_directory() . '/plugins/phpgooglespreadsheetapi/table-getter.php';

    $output = array (is_object($TableGetter) ? $TableGetter->execute( $params ) : '500');

    echo json_encode( $output );

    exit;
}

add_action( 'wp_ajax_init_table_getter', 'init_table_getter' );
add_action( 'wp_ajax_nopriv_init_table_getter', 'init_table_getter' );
