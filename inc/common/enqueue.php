<?php
/**
 * Enqueue assets.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


/**
 * Enqueue scripts and styles.
 */
function wordpress_kit_scripts() {
	wp_enqueue_style( 'theme-main', get_template_directory_uri() . '/assets/css/main.min.css', array(), '2.1', 'all' );
	wp_enqueue_style( 'theme-responsive', get_template_directory_uri() . '/assets/css/main.min.responsive.css', array(), '2.1', '(max-width:1250px)' );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/assets/js/main.min.js', array(), '2.1', true );
	//wp_enqueue_script( 'googme-maps-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCRJz_DMLCdqsfRwyysbIaWvzDxIIKzmaU', array(), '2.1', true );
}
add_action( 'wp_enqueue_scripts', 'wordpress_kit_scripts' );


/**
 * Defer attribute to scripts
 */
function add_defer_attribute($tag, $handle) {

   // add script handles to the array below
   $scripts_to_defer = array( 'wp-embed', 'theme-js', 'googme-maps-js' );
   
   foreach($scripts_to_defer as $defer_script) {
      if ($defer_script === $handle) {
         return str_replace(' src', ' defer="defer" src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


/**
 * Include AJAX
 */
function global_js_variables(){
	global $post;

	wp_localize_script('theme-js', 'global_var', 
		array(
			'url'             => admin_url('admin-ajax.php'),
			'post_id'         => $post ? $post->ID : 0,
			'page_title'      => $post ? get_the_title() : 'Title undefined',
			'ajaxnonce'       => wp_create_nonce( "ajax_nonce" ),
			'google_maps_key' => 'AIzaSyCRJz_DMLCdqsfRwyysbIaWvzDxIIKzmaU',
			'theme_url'       => get_template_directory_uri(),
			'home_url'        => get_bloginfo('home_url')
		)
	);  
}
add_action( 'wp_enqueue_scripts', 'global_js_variables', 99 );

?>