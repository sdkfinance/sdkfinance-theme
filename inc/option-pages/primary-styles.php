<?php 
/**
 * Option page: Primary Styles
 *
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


if( function_exists('acf_add_options_sub_page') ) :

	$primary_content = acf_add_options_page(array(
		'page_title'   => 'Theme Styles',
		'menu_title'   => 'Theme Styles',
		'menu_slug'    => 'acf-theme-styles',
		'icon_url'     => 'dashicons-align-left',
		'post_id'      => 'styles',
		'redirect'     => false,
	));

endif;