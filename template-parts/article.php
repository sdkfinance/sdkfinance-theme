<?php
/**
 * Component: Article
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */


$d            = array();
$d['title']   = get_the_title();
$d['content'] = get_the_excerpt();
$d['date']    = get_post_metadata();
$d['url']     = get_permalink();
$d['thumb']   = get_the_post_thumbnail( $post->ID, '850-size' );

/**
 * Get article view
 */
include THEME_VIEWS . 'loop/article.php';