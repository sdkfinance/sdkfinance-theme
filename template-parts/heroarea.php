<?php
/**
 * Component: Row
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common;
$d                 = array();
$attrs             = array();
$style             = array();
$classes           = array();
$classes[]         = 'heroarea'; 

$d['title']        = '';

if ( is_home() ) 
	$d['title']    = '<h1 class="heroarea--title is-brand-gradient">Blog</h1>';
elseif ( is_singular( 'post' ) )
	$d['title']    = '<h2 class="heroarea--title is-brand-gradient">Blog</h2>';
elseif ( is_archive() ) 
	$d['title']    = '<h1 class="heroarea--title is-brand-gradient">'.get_the_archive_title().'</h1>';
else 
	$d['title']    = '<h1 class="heroarea--title is-brand-gradient">'.get_the_title().'</h1>';

$d['bg'] = get_field( 'bg-heroarea' ) ? get_field( 'bg-heroarea' ) : '';
$style[] = $d['bg'] ? "background-image:url({$d['bg']});" : '';

// Compile classes and attributes
$attrs[]           = count($style) > 0 ? 'style="' . generate_classlist( $style ) . '"' : '';
$attrs[]           = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs             = generate_classlist( $attrs );

include THEME_VIEWS . 'core/heroarea.php';	