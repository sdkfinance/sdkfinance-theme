<?php
/**
 * Component: Row
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$attrs             = array();
$style             = array();
$classes           = array();
$classes[]         = 'row'; 
$classes[]         = 'box'; 
$classes[]         = 'box-inner'; 


// Options
$d['cols_count']      = get_sub_field( 'cols-count' ); 
$d['layout']          = get_sub_field( 'layout' ); 
$d['cols_margins']    = get_sub_field( 'cols-margins' ); 
$d['width']           = get_sub_field( 'width' ); 
$d['position']        = get_sub_field( 'position' ); 
$d['cols_placement']  = get_sub_field( 'cols-placement' ); 
$d['margin_top']      = get_sub_field( 'margin_top' ); 
$d['margin_bottom']   = get_sub_field( 'margin_bottom' ); 

$classes[]            = 'layout_' . $d['layout'];
$classes[]            = 'width_' . $d['width'];
$classes[]            = 'position_' . $d['position'];
$classes[]            = 'cols_placement_' . $d['cols_placement'];

$d['margin_top']      = $d['margin_top'] / 10;
$d['margin_bottom']   = $d['margin_bottom'] / 10;

$style[]              = $d['margin_top'] ? "margin-top:{$d['margin_top']}rem;" : '';
$style[]              = $d['margin_bottom'] ? "margin-bottom:{$d['margin_bottom']}rem;" : '';
$classes[]            = 'cols_margins_' . $d['cols_margins'];

// Grid
if ( $d['layout']  == 'grid' ) :
	$d['cols_align']      = get_sub_field( 'cols-align' ); 
	$d['cols_ratio']      = get_sub_field( 'cols-ratio' ); 

	$classes[]            = 'cols_align_' . $d['cols_align'];
	$classes[]            = 'cols_count_' . $d['cols_count'];

	if ( $d['cols_count'] == 2 ) :
		$classes[]        = 'cols_ratio-' . $d['cols_ratio'];
	endif;
endif;


// Carousel
if ( $d['layout']  == 'carousel' ) :
	$d['slides_scroll']   = get_sub_field( 'slides_scroll' ); 
	$d['show_dots']       = get_sub_field( 'show_dots' ); 

	$attrs[]              = "data-scroll='{$d['slides_scroll']}'";
	$attrs[]              = "data-columns='{$d['cols_count']}'";
	$attrs[]              = "data-dots='{$d['show_dots']}'";
endif;



// Compile classes and attributes
$attrs[]    = count($style) ? 'style="' . generate_classlist( $style ) . '"' : '';
$attrs[]    = count($classes) ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

/**
 * Get section view
 */
include THEME_VIEWS . 'core/row.php';