<?php
/**
 * Component: Primary Content
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

if ( have_rows( 'primary_content' ) ) :
	$section_count  = count( get_field('primary_content') );
	$section_lack   = $section_count - 1;
	$GLOBALS['section_number'] = 0;
	?>

	<div 
		id="primary-content"
		class="primary-content__wrapper"
		data-status="1"
		data-field="primary_content"
		data-offset="5"
		data-lack="<?=$section_lack;?>"
		data-method="ajax_acf_load">

		<?php
	
		while ( have_rows( 'primary_content' ) ) :
			$the_section_row = the_row();
			$section_id      = 'pr-co';
			$GLOBALS['section_number']++;

			/**
			 * Get section template part
			 */
			include get_template_directory() . '/template-parts/' . get_row_layout() . '.php';

			//break;
		endwhile;
		?>

	</div>

	<?php
endif;

