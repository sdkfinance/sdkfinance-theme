<?php
/**
 * Component: Section
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$attrs             = array();
$style             = array();
$classes           = array();
$classes[]         = 'section';

// View
$vertical_paddigns = get_sub_field('vertical_paddigns');
$color             = get_sub_field('color');
$background_image  = get_sub_field('background_image');

$classes[]         = $vertical_paddigns ? "paddings_$vertical_paddigns" : '';
$classes[]         = "color_$color";
$style[]           = $background_image ? "background-image: url($background_image);" : '';

// Options
$section_id        = get_sub_field('section_id');
$attrs[]           = $section_id ? "id='{$section_id}'" : '';

if ( get_sub_field('css-snippets') ) 
	add_action('before_open_section_tag', function(){
		$section_css = get_sub_field('css-snippets') ? "<style type='text/css'>" . get_sub_field('css-snippets') . "</style>" : '';;
		echo $section_css;
	}, 15);

// Compile classes and attributes
$attrs[]           = count($style) > 0 ? 'style="' . generate_classlist( $style ) . '"' : '';
$attrs[]           = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs             = generate_classlist( $attrs );

/**
 * Get section view
 */
include THEME_VIEWS . 'core/section.php';