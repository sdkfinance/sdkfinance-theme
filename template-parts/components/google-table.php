<?php
/**
 * Component: Google Table
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();

$d['attrs']        = array();
$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';
$attrs[]           = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';

$classes           = array();
$classes[]         = 'google-table';

$attrs[] = get_sub_field('spreadsheetId') ? 'data-table-id="' . get_sub_field('spreadsheetId') . '"' : '';
$attrs[] = get_sub_field('worksheetName') ? 'data-table-name="' . get_sub_field('worksheetName') . '"' : '';

// Compile classes and attributes
$attrs[]    = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

echo "<div {$attrs}>Loading</div>";