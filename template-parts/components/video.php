<?php
/**
 * Component: Video
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();

$d['attrs']        = array();
$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';
$attrs[]           = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';

$classes           = array();
$classes[]         = 'embed-container';

$d['content']      = get_sub_field( 'video' );

// use preg_match to find iframe src
preg_match('/src="(.+?)"/', $d['content'], $matches);
$src = isset($matches[1]) ? $matches[1] : '#';

// add extra params to iframe src
$params = array(
    'controls'    => 1,
    'hd'        => 1,
    'autohide'    => 1,
    'loop' => 1,
    'rel' => 0
);
$new_src = add_query_arg($params, $src);
$d['content'] = str_replace($src, $new_src, $d['content']);

// add extra attributes to iframe html
$attributes = 'allowfullscreen';
$d['content'] = str_replace('frameborder="0" gesture="media" allowfullscreen></iframe>', ' ' . $attributes . '></iframe>', $d['content']);

// Compile classes and attributes 
$attrs[]    = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

/**
 * Get section view
 */
echo "<div {$attrs}>{$d['content']}</div>";