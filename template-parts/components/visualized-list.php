<?php
/**
 * Component: Visualized list
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */
?>

<div class="visualized-list__box">
	<?php
	// Common
	$d              = array();
	$attrs          = array();
	$classes        = array();

	$d['type']      = get_sub_field( 'type' );
	$d['sublist']   = get_sub_field( 'sublist' );
	$d['layout']    = get_sub_field( 'layout' );
	$d['offset']    = get_sub_field( 'offset' ) ? get_sub_field( 'offset' ) : 'auto';

	$classes[]      = 'visualized-list';
	$classes[]      = $d['type'] ? "type_{$d['type']}" : '';
	$classes[]      = $d['offset'] ? "offset_{$d['offset']}" : '';
	$classes[]      = $d['layout'] ? "layout_{$d['layout']}" : '';

	$d['title']     = get_sub_field( 'title' ) ? get_sub_field( 'title' ) : '';
	$d['title']     = $d['title'] ? "<h3 class='visualized-list__headline is-brand-back-medium offset_{$d['offset']}'>{$d['title']}</h3>" : '';

	$d['attrs']        = array();
	$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
	$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';
	$attrs[]           = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';

	$attrs[]           = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
	$attrs             = generate_classlist( $attrs );

	if ( have_rows( 'visualized-list' ) ) :
		echo $d['title']; // echo title
		?>

		<ul <?=$attrs;?>>

			<?php
			/**
			 * Loop list
			 */
			while ( have_rows( 'visualized-list' ) ) :
				the_row();

				$d['sublist']  = array();
				$d['icon']     = get_sub_field('icon') ? get_sub_field('icon') : get_template_directory_uri() . '/assets/img/icons/check.png';
				$d['textarea'] = get_sub_field('textarea');

				// Sublist grab data
				if ( have_rows( 'sub-list' ) ) :
					$d['counter'] = 0;

					/**
					 * Loop sub-list
					 */
					while ( have_rows( 'sub-list' ) ) :
						the_row();
						$c = $d['counter']++;

						$d['sublist'][$c]['textarea'] = get_sub_field( 'textarea' );
					endwhile;
				endif;

				include THEME_VIEWS . 'common/visualized-list.php';
			endwhile;
			?>

		</ul>

		<?php
	endif;
	?>
</div>