<?php
/**
 * Component: Image
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$classes           = array();
$attrs             = array();

$classes[]         = 'image-component';
$classes[]         = get_sub_field( 'align' ) ? 'align_' . get_sub_field( 'align' ) : '';
$classes[]         = get_sub_field( 'vertical' ) ? 'vertical_' . get_sub_field( 'vertical' ) : '';

$d['attrs']        = array();
$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'max_width' ) ? 'max-width:' . get_sub_field( 'max_width' ) . ';' : '';

$d['image']        = get_sub_field( 'image' );
$d['mime_type']    = $d['image']['mime_type'];

switch ( get_sub_field( 'type' ) ) :
	case 'url':
		$d['href'] = get_sub_field( 'url' );
		$attrs[]   = 'rel="nofollow"'; 
		$attrs[]   = 'target="_blank"'; 
		break;

	case 'page':
		$d['href'] = get_sub_field( 'page' );
		break;

	case 'popup':
		$d['href'] = $d['image']['url'];
		$classes[] = 'js-popup-image';
		break;

	default:
		$d['href'] = '';
		break;
endswitch;

$attrs[]    = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';
$attrs[]    = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

$d['open']  = $d['href'] ? "<a href='{$d['href']}' {$attrs}>" : "<div {$attrs}>";
$d['close'] = $d['href'] ? "</a>" : '</div>';

/**
 * Get section view
 */
include THEME_VIEWS . 'common/image.php';