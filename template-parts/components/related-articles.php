<?php
/**
 * Component: Related articles
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

$tags = wp_get_post_tags( $post->ID );

if ( $tags ) :
	$tag_ids = array();

	foreach ( $tags as $individual_tag ) 
		$tag_ids[] = $individual_tag->term_id;

	$args = array (
		'tag__in'      => $tag_ids, 
		'post__not_in' => array($post->ID),
		'showposts'    => 2
	);

	$tag_query = new wp_query( $args );

	if ( $tag_query->have_posts() ) : 
		?>

		<div class="section paddings_small is-bg-brand-back-light">
			<h5 class="is-brand-back-medium box box-inner">You may also like</h5>
			
			<div class="archive-loop_shortly box box-inner">

				<?php
		        while ( $tag_query->have_posts() ) :
		            $tag_query->the_post();

					$d            = array();
					$d['title']   = get_the_title();
					$d['date']    = get_post_metadata();
					$d['title']   = get_the_title();
					$d['date']    = get_post_metadata();
					$d['url']     = get_permalink();
					$d['thumb']   = get_the_post_thumbnail( $post->ID, '850-size' );

					/**
					 * Get section view
					 */
					include THEME_VIEWS . 'loop/article-shortly.php';
		        endwhile;
		        ?>

	        </div>
		</div>
		
        <?php
    endif;
endif;
?>
