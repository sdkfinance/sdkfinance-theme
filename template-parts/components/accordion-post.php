<?php
/**
 * Component: Accordion
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$classes           = array();
$classes[]         = 'accordion accordion-post box box-inner';
$d['styles']       = array('padding-bottom: 3rem;');

$d['content']      = get_field( 'accordion-list' );

// Compile classes and attributes
$attrs[]    = count( $d['styles'] ) > 0 ? 'style="' . generate_classlist( $d['styles'] ) . '"' : '';
$attrs[]    = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = count( $attrs ) > 0 ? generate_classlist( $attrs ) : '';

if ( is_array($d['content']) )
	include THEME_VIEWS . 'common/accordion.php';