<?php
/**
 * Component: Row
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                  = array();
$d['attrs']         = array();

$d['attrs'][]       = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]       = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

$d['attrs']         = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';

$d['open']		    = "<div class='cards-list' {$d['attrs']}>";
$d['close']		    = '</div>';

if ( have_rows( 'cards' ) ) :
	echo $d['open'];

	while ( have_rows( 'cards' ) ) :
		the_row();

		$d['icon']        = get_sub_field('icon');
		$d['title']       = get_sub_field('title');
		$d['sub-title']   = get_sub_field('sub-title');
		$d['details']     = get_sub_field('details');
		$d['sub-details'] = get_sub_field('sub-details');

		include THEME_VIEWS . 'common/card.php';
	endwhile;

	echo $d['close'];
endif;
