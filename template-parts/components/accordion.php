<?php
/**
 * Component: Accordion
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$classes           = array();
$classes[]         = 'accordion';
$d['styles']       = array();

$d['styles'][]     = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['styles'][]     = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

$d['content']      = get_sub_field( 'accordion-list' );

// Compile classes and attributes
$attrs[]    = count( $d['styles'] ) > 0 ? 'style="' . generate_classlist( $d['styles'] ) . '"' : '';
$attrs[]    = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = count( $attrs ) > 0 ? generate_classlist( $attrs ) : '';

if ( is_array($d['content']) )
	include THEME_VIEWS . 'common/accordion.php';