<?php
/**
 * Component: Logos gallery
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$d['attrs']        = array();

$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

$classes           = array();
$classes[]         = 'logos-gallery';

// View
$d['placement']    = get_sub_field( 'placement' );
$classes[]         = $d['placement'];
 
// Data
$d['gallery']      = get_sub_field( 'logos-gallery' );

// Compile classes and attributes

$attrs[]    = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';
$attrs[]    = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

/**
 * Get section view
 */
include THEME_VIEWS . 'common/logos-gallery.php';