<?php
/**
 * Component: Image
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d            = array();
$classes      = array();
$attrs        = array();
$d['style']   = array();

$d['type']       = get_sub_field( 'type' );
$d['id']         = get_sub_field( 'id' );
$d['submit']     = get_sub_field( 'submit-label' ) ? get_sub_field( 'submit-label' ) : 'Submit';
$d['confirm']    = get_sub_field( 'confirmation-page' ) ? get_sub_field( 'confirmation-page' ) : '';
$d['disclaimer'] = get_sub_field( 'disclaimer' ) ? get_sub_field( 'disclaimer' ) : '';

$classes[]    = 'form';
$classes[]    = "form-type__{$d['type']}";

$d['style'][] = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['style'][] = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

$attrs[]      = "id='{$d['id']}'";
$attrs[]      = "data-confirmation-page='{$d['confirm']}'";

// special things for paypal form types
if ( $d['type'] == 'pay-for-instance' ) :
	$attrs[] = 'data-pp="1"';
	$attrs[] = 'action="https://www.paypal.com/cgi-bin/wenscr"';
	$attrs[] = 'method="post"';
endif;

$attrs[]      = count( $d['style'] ) > 0 ? 'style="' . generate_classlist( $d['style'] ) . '"' : '';
$attrs[]      = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs        = apply_filters( 'sdk_form-attrs', $attrs );
$attrs        = generate_classlist( $attrs );

include THEME_VIEWS . "form/{$d['type']}.php";