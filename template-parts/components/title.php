<?php
/**
 * Component: Title
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$d['attrs']        = array();
$classes           = array();

$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

// View
$d['tag']          = get_sub_field( 'tag' );
$d['color']        = get_sub_field( 'color' );
$d['align']        = get_sub_field( 'align' );
$d['weight']       = get_sub_field( 'weight' );
$d['styling']      = get_sub_field( 'styling' );

$classes[]         = $d['color'] ? $d['color'] : '';
$classes[]         = $d['align'] ? $d['align'] : '';
$classes[]         = $d['weight'] ? $d['weight'] : '';
$classes[]         = $d['styling'] ? $d['styling'] : '';

// Data
$d['title']        = get_sub_field( 'title' );

// Compile classes and attributes
$attrs[]           = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';
$attrs[]           = count( $classes ) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs             = generate_classlist( $attrs );

echo "<{$d['tag']} {$attrs}>{$d['title']}</{$d['tag']}>";