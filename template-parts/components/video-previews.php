<?php
/**
 * Component: Video previews
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$d['attrs']        = array();

$d['attrs'][]      = get_sub_field( 'margin_top' ) ? 'margin-top:' . ( get_sub_field( 'margin_top' ) / 10 ) . 'rem;' : '';
$d['attrs'][]      = get_sub_field( 'margin_bottom' ) ? 'margin-bottom:' . ( get_sub_field( 'margin_bottom' ) / 10 ) . 'rem;' : '';

$attrs[]           = count( $d['attrs'] ) > 0 ? 'style="' . generate_classlist( $d['attrs'] ) . '"' : '';
$attrs             = generate_classlist( $attrs );

if ( have_rows( 'video_previews' ) ) :
	?>

	<div class="video-previews" <?=$attrs;?>>
		<div class="video-previews__list">

			<?php
			while ( have_rows( 'video_previews' ) ) :
				the_row();

				$d['content']     = get_sub_field('content');

				include THEME_VIEWS . 'common/video-preview.php';
			endwhile;
			?>

		</div>
	</div>

	<?php
endif;