<?php
/**
 * Component: Row Template
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$row_object = get_sub_field( 'row-template' );

if ( $row_object ) : 

	// override $post
	$post = $row_object;
	setup_postdata( $post );

	if ( have_rows( 'row-template', $post->ID ) ) :
		while ( have_rows( 'row-template', $post->ID ) ) :
			$the_section_row = the_row();
			$section_id      = 'row-tem';

			include get_template_directory() . '/template-parts/row.php';
		endwhile;
	endif;

	wp_reset_postdata();
endif;