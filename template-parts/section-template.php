<?php
/**
 * Component: Section
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$section_object = get_sub_field( 'section-template' );

if ( $section_object ) : 

	// override $post
	$post = $section_object;
	setup_postdata( $post );

	/**
	 * Check whether Section template post has a sections.
	 * Pull that if it exists
	 */
	if ( have_rows( 'primary-content-template', $post->ID ) ) :
		while ( have_rows( 'primary-content-template', $post->ID ) ) :
			$the_section_row = the_row();
			$section_id      = 'sec-tem';

			include get_template_directory() . '/template-parts/section.php';
		endwhile;
	endif;

	wp_reset_postdata();
	
/**
 * In this case we dont need to define $post->id
 */
elseif ( is_singular( 'template' ) ) :
	if ( have_rows( 'primary-content-template' ) ) :
		while ( have_rows( 'primary-content-template' ) ) :
			$the_section_row = the_row();
			$section_id      = 'sec-tem';

			include get_template_directory() . '/template-parts/section.php';
		endwhile;
	endif;
endif;