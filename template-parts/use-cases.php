<?php
/**
 * Component: Use cases
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

$d          = array();

/**
 * Get section view
 */
include THEME_VIEWS . 'loop/use-cases.php';