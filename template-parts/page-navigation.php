<?php
/**
 * Component: Section
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$d                 = array();
$attrs             = array();
$style             = array();
$classes           = array();
$classes[]         = 'section';
$classes[]         = 'page-navigation';

// View
if ( have_rows('page-navigation') ) :
	while ( have_rows('page-navigation') ) :
		the_row();
		$label  = get_sub_field('label');
		$target = get_sub_field('id');

		$d['content'][$label] = "#{$target}";
	endwhile;
endif;

// Compile classes and attributes
$attrs[]    = count($style) > 0 ? 'style="' . generate_classlist( $style ) . '"' : '';
$attrs[]    = count($classes) > 0 ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs      = generate_classlist( $attrs );

/**
 * Get section view
 */
include THEME_VIEWS . 'core/page-navigation.php';