<?php
/**
 * Component: Row
 * 
 * @package Sdk.Finance_Theme
 * @author  Saveliy D. (dzvonkevich@gmail.com)
 */

// Common
$attrs             = array();
$classes           = array();
$classes[]         = 'widgets';


// Compile classes and attributes
$attrs[] = count($classes) ? 'class="' . generate_classlist( $classes ) . '"' : '';
$attrs   = generate_classlist( $attrs );

/**
 * Get section view
 */
include THEME_VIEWS . 'core/widgets.php';