<?php
/**
 * Single page sidebar template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

$author_mail = get_the_author_meta( 'email' );

$d           = array();
$d['avatar'] = get_avatar_url( 
	$author_mail, 
	array('size' => 150)
);
$d['category'] = get_the_category();

?>

<div class="sidebar">
	<div class="sidebar-widget wysiwyg">
		<div class="sidebar-widget__meta">
			<?php if ( $d['avatar'] ) : ?>
				<div class="sidebar-widget__author--thumb">
					<img src="<?=$d['avatar'];?>" alt="" />
				</div>
			<?php endif; ?>
			
			<p><?php echo get_post_metadata(); ?></p>
		</div>
	</div>

	<?php 
	if ( ! wp_is_mobile() && $d['category'] ) :
		?>

		<div class="sidebar-widget wysiwyg">
			<h2 class="h6">Categories</h2>
			<ul>
				<?php 
				foreach( $d['category'] as $category ) :
					$cat_link = get_category_link( $category->cat_ID );
					?>

					<li><a href="<?=$cat_link;?>"><?php echo $category->cat_name; ?></a></li>
					
					<?php 
				endforeach; 
				?>
			</ul>
		</div>

		<?php
	endif;
	?>
</div>