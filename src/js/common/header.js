/*  =========================
	Header */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each acroll to anchor
		 */
		init: function() {
            var $header    = $(this).not('[data-inited="1"]');	
            var $menu      = $header.find('.header-primary--menu');

            if ( $header.length > 0 ) {

                // Burger clicking
                $header.on('click', '.header-primary--burger', () => {
                	let $burger = $(this);

                	$burger.toggleClass('is-active'); 
                	$menu.slideToggle();

                	$(window).on('scroll', () => {
    	            	$burger.removeClass('is-active'); 
    	            	$menu.slideUp();
                	});
                });

                // sticky header
                let height    = $header.height();

                if (!$header.hasClass('position_overlay')) {
                    $('main').animate({'margin-top': height + 'px'}, 300);
                }

                $(window).on('scroll', function () {
                    let position   = $(this).scrollTop();

                    // Fix on top of window
                    if (position > $('#wrapper').offset().top) {
                        $header.addClass('js-scroll');
                        $header.addClass('fixed');
                    } else {
                        $header.removeClass('js-scroll');
                        $header.removeClass('fixed');
                     }
                });

                $header.attr('data-inited', 1);
            }
		}

	};

	/** 
	 * Init method
	 */
	$.fn.header = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.header' );
        } 

    };

}));