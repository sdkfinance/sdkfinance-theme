/*  =========================
	Form */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	function precisionRound(number, precision) {
		const factor = Math.pow(10, precision);
		return Math.round(number * factor) / factor;
	}

	const methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			const $form = $(this).not('[data-inited="1"]');

			// Hand up the submit event
			if ($form.length > 0) {
        $form.form('submit');

				if ($form.find('input[type="file"]').length) {
          $form.form('initFileInput');
				}
			}

			$form.attr('data-inited', 1);
		},

    initFileInput: function () {
			const $form  = $(this);
			const $file  = $form.find('[type="file"]');
      const $label = $form.find('.file-upload__label');

      $file.on('change', e => {
        const file   = e.currentTarget.files[0];
        const sizeMb = precisionRound(file.size / 1024 / 1024, 2);

        if (file.name.match( /.jpeg|.png|.jpg/i ) === null) {
          $label.html(`<span style="color:red;">Load an image please</span>`);
          e.currentTarget.value = '';
          setTimeout(() => $label.html(`No logo chosen`), 3000);

        } else if (sizeMb > 1) {
          $label.html(`<span style="color:red;">${sizeMb} MB of 1 MB allowed</span>`);
          e.currentTarget.value = '';
          setTimeout(() => $label.html(`No logo chosen`), 3000);

				} else {
          let formData = new FormData(this[0]);
          formData.append('logotype', file);
          formData.append('action', 'upload_file_from_form');

          $.ajax({
            method: 'POST',
            url: global_var.url,
						contentType: false,
            processData: false,
            cache : false,
            async : false,
            data: formData,
            beforeSend: () => {
              $label.form('toggleSpiner');
            },
            complete: resp => {
              const responce = JSON.parse(resp.responseText);

              $label.form('toggleSpiner').text(`${sizeMb} MB | ${file.name}`);
              $file.attr('data-url', responce.status.url);
						}
          });
				}
			});
		},

		submit: function () {
			const $form = $(this);

			$form.on('submit', function (e) {
				const values  = {};
        const $inputs = $(this).find('.form-body').find('input');
        const is_pp   = $(this).attr('data-pp') === '1';
        const is_wp   = $(this).hasClass('form-type__whitepaper'); // wp - white paper
				let   error   = false;
				const request = {};
        const $button = $(this).find('[type="submit"]');

        if (is_pp) {
          request.action = 'log_filled_form_paypal';
        } else if (is_wp) {
          request.action = 'log_filled_form_whitepaper';
        } else {
          request.action = 'log_filled_form';
        }

				$inputs.map(function(){
					const $this = $(this);
					const valid = $this.form('isValidField');

					// Either add or remove warning message
					if (valid.is) {
						$this.parent().find('.form-field_error').text('');
					} else {
						$this.parent().find('.form-field_error').text(valid.msg);
						error = true;
					}
					
					// add value to gllbal array
					values[$this.attr('name')] = $this.attr('type') !== 'file' ? $this.val() : $this.attr('data-url');
				});

				if (error) {
					e.preventDefault();
					return false;
				}

				request['page'] = global_var.page_title;

				// Log new request	
				for (let key in values)
        	request[key] = values[key];

        $.ajax({
            method: 'POST',
            url: global_var.url,
            data: request,
            beforeSend: () => {
            	$button.form('toggleSpiner');
						},
						complete: resp => {
            	const result = JSON.parse(resp.responseText);

							if (is_pp) {
								console.log('Paypal form');
							} else {
                e.preventDefault();

								// throw an user to a confirmation page
				        document.location.href = $form.attr('data-confirmation-page') || global_var.home_url;
							}
            }
        });

        if (!is_pp)
        	return false;
			});
		},

		toggleSpiner: function () {
			const $button = $(this);

			if ($button.length > 0) {
				const isLoading = $button.hasClass('btn-loading');

				if (!isLoading) {
					$button
						.attr('data-label', $button.text())
						.html('<div class="spinner btn-loader"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>')
						.addClass('btn-loading');

				} else {
					$button
						.html($button.attr('data-label'))
						.removeClass('btn-loading');
				}
			}

			return $button;
		},

		isValidField: function () {
			const $field = $(this);
			const isFile = this[0].type === 'file';
			const value  = $field.val();
			const valid  = {
				is: true,
				msg: ''
			};

			if (isFile && !$field.attr('data-url')) {
        console.warn("Empty logotype field");
			} else if (value === '') {
				valid.is  = false;
				valid.msg = "It's required field.";
			} else {
				if ($field.hasClass('valid-email')) {
					let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    			
	    			if (!re.test(value)) {
	    				valid.is  = false;
	    				valid.msg = "Fill valid email adress please.";
	    			}
				} else if ($field.hasClass('valid-phone')) {
          let re = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

          if (!re.test(value)) {
            valid.is  = false;
            valid.msg = "Fill valid phone number please. For instance, 123 456 7899";
          }
				}
			}

			return valid;
		}
	};

	/** 
	 * Init method
	 */
	$.fn.form = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.form' );
        } 

    };

}));