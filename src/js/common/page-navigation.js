/*  =========================
	Page navigation */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			var $this = $(this).not('[data-inited="1"]');

			try {
				if ($this.length > 0) {

					// Add wrapper 
					if ($this.hasClass('page-navigation') && !$this.hasClass('js-page-navigation')) {
						$this.wrap('<div class="js-page-navigation"></div>');
						var height = $this.closest('.js-page-navigation').height();
					} else if (!$this.hasClass('js-page-navigation')) {
						$this.addClass('js-page-navigation');
						var height = $this.closest('.js-page-navigation').height();
					}

					// Common
					const anchors    = [];
					let sections     = [];
					let $fixedHeader = $('.header-primary.js-sticky');
					const offsetTop  = $this.offset().top;

					//DOM
					let $links = $this.find('a');

					$links.each(function(){
						const $self = $(this);
						const href  = $self.attr('href');

						anchors.push(href);
					});

					// Fix layout
					$('.js-page-navigation').height(height);

					// Handle window scrolling
					$(window).on('scroll', () => {
						let headerHeight = +$fixedHeader.height();
                    	let headerOffset = $fixedHeader.length && +$fixedHeader.offset().top + headerHeight;
						let position     = headerOffset || $(window).scrollTop();

						// Fix on top of window
						if (position >= offsetTop) {
							$this.addClass('is-fixed');

							headerOffset && $this.css({'transform': 'translateY('+headerHeight+'px)'});
						} else {
							$this.removeClass('is-fixed');

							headerOffset && $this.css({'transform': 'translateY(0)'});
						}

						// Handle anchors 
						anchors.forEach(element => {
							const $element      = $(element);

							if ($element.length == 1) {
								const $elementLink  = $this.find('a[href="' + element + '"]');
								const elementHeight = $element.height();
								const elementTop    = $element.offset().top;
								const elementEnd    = elementTop + elementHeight;

								if (position >= elementTop && position <= elementEnd) {
									$elementLink.addClass('is-active');
								} else {
									$elementLink.removeClass('is-active');
								}
							}
						});
					});
				} 
			} catch (e) {
				console.log(e);
			}

			$this.attr('data-inited', 1);
		}

	};

	/** 
	 * Init method
	 */
	$.fn.pageNavigation = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.pageNavigation' );
        } 

    };

}));