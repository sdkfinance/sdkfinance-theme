/*  =========================
	Sidebar */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			const $sidebars = $(this).not('[data-inited="1"]');
			const initialize = function () {
        // wrapper
        var $sidebar          = $(this);
        var $wrapper          = $sidebar.closest('.area--sidebar');
        var wrapperTop        = Math.round($wrapper.offset().top);
        var wrapperWidth      = $wrapper.width();
        var wrapperHeight     = $wrapper.height();
        var sidebarPaddingTop = wrapperTop - 50;
        var bottomEdgePoint   = wrapperTop + wrapperHeight;

        console.dir($sidebar);

        // Extra sections inside an article
        var $sections         = $('.single-area').find('.section');

        // Rewrite Bottom Endpoint
        // if there are any sections
        if ($sections.length > 0) {
          var $section          = $sections.eq(0);
          var sectionTop        = Math.round($section.offset().top);
          var sectionPaddingTop = sectionTop - 50;
          var sectionHeight     = $section.height();

          bottomEdgePoint      = sectionPaddingTop;
          wrapperHeight        = sectionPaddingTop - wrapperTop;
          $wrapper.height(wrapperHeight);
        } else {
          var $section          = undefined;
          var sectionTop        = undefined;
          var sectionPaddingTop = undefined;
          var sectionHeight     = undefined;
        }

        // sidebar
        const sidebarHeight = $sidebar.height();

        $sidebar
					.wrap('<div class="absolute"></div>')
					.width(wrapperWidth)
					.height(sidebarHeight)
					.parent()
					.width(wrapperWidth)
					.height(wrapperHeight);

        $(window).on('scroll', function () {
          let scrollTop      = $(this).scrollTop();
          let sidebarTop     = $sidebar.offset().top;
          let sidebarEndTop  = sidebarTop + sidebarHeight;
          const headerHeight = $('.header-primary').filter('.js-scroll').height() || 0;

          if (headerHeight)
            scrollTop += headerHeight;

          // Put initial state when an user
          // scrolled to top of page
          if (scrollTop < sidebarPaddingTop) {
            $sidebar.sidebar('makeStatic');

            // Actions after an user's top edge
            // of screen pass the height point of sidebar
          } else {

            // Activate fixed state
            if (sidebarEndTop < bottomEdgePoint) {
              $sidebar.sidebar('makeFixedTop');

              // Fix sidebar on bottom of wrapper
              // to avoid the sidebar overlay on something
            } else {
              $sidebar.sidebar('makeAbsoluteBottom');

              // back to fixed state
              // if an user scrolled to top
              if ( (sidebarTop - 50) > scrollTop)
                $sidebar.sidebar('makeFixedTop');
            }
          }
        });

        $sidebar.attr('data-inited', 1);
			};

			if ($sidebars.length > 0 && $(window).width() > 768) {
				var $body = $('#body-class');

				// Scri[t for post page
				if ($body.hasClass('single-post') || $body.hasClass('page-template-tpl-whitepaper')) {
          $sidebars.each(key => initialize.call($sidebars[key]));
				} 
			}
		},

		makeFixedTop: function () {
			let top = 5;
			let headerHeight = $('.header-primary').filter('.js-scroll').height() || 0;

			if (headerHeight)
				top += headerHeight / 10;

			$(this).css({
				'position': 'fixed',
				'top': top + 'rem',
				'bottom': 'auto'
			});
		},

		makeAbsoluteBottom: function () {
			$(this).css({
				'position': 'absolute',
				'top': 'auto',
				'bottom': 0
			});
		},

		makeStatic: function () {
			$(this).css({
				'position': 'static',
				'top': 'auto',
				'bottom': 'auto'
			});
		}

	};

	/** 
	 * Init method
	 */
	$.fn.sidebar = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.sidebar' );
        } 

    };

}));