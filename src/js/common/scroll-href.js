/*  =========================
	Scroll Href */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each acroll to anchor
		 */
		init: function () {
            $(this).click( function () { 
                const href        = $(this).attr('href'); 
                const $element    = $(href);
                const $fixedHeader = $('.header-primary');
                const $fixedNav    = $('.page-navigation');

                let offset = 1;

                if ( $element.length != 0 ) {
                    offset += $element.offset().top;
                    offset -= $fixedHeader.height();

                    if ( $fixedNav.length > 0 ) {
                        offset -= $fixedNav.height();
                    }

                    $('html, body').animate({ scrollTop: offset }, 500); 
                }
                
                return false;
            });		
		}

	};

	/** 
	 * Init method
	 */
	$.fn.scrollHref = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.scrollHref' );
        } 

    };

}));