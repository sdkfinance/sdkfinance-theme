/*  =========================
	Primary content */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			var $primaryContent = $('.section');

			/* Update all sliders heights */
			$('.slick-slider').slick('setOption', 'height', null, true);

			$primaryContent.each(function(){
				var $self = $(this);

				/**
				 * Check whether that section wasn't loaded
				 */
				if (!$self.hasClass('js-handled')) {


					/**
					 * Set rows' backgrounds
					 */
					var $rows = $self.find('.rows');
					if ($rows.children().length > 0) {
						$rows.primaryContent( 'initRows' );
					}

					/**
					 * Init panel navigation if that section contain that
					 */
					if ($self.hasClass('js-panel-navigation')) {
						$self.pageNavigation('init');
					}

					/**
					 * Stabilize that section 
					 * if it is pasted everywhere in content
					 */
					if ($self.parent('.wysiwyg').length > 0) {
						var $parent         = $self.parent();
						var parentWidth     = $parent.width();
						var windowWidth     = $(window).width();
						var selfOffsetLeft  = $self.offset().left;
						var selfOffsetRight = windowWidth - parentWidth - selfOffsetLeft;

						$self.css({
							'margin-left': selfOffsetLeft * -1,
							'margin-right': selfOffsetRight * -1,
						});
					}

					// if it's pricing section
					// init slider
					if ( $self.attr('id') === 'price' ) {
						$self.primaryContent('initPricingCarousel');
					}

					/**
					 * Add indicator-class to avoid reworking 
					 * that file during ajax request
					 */
					$self.addClass('js-handled');
				}
			});
		},

		/**
		 * Init rows' functions
		 */
		initRows: function () {
			var $this = $(this);
			var $rows = $this.children();

			/**
			 * Handle each child-row
			 */
			$rows.each(function(){
				var $row = $(this);

				// TODO: functions load more
				
				/**
				 * Build carousel
				 */
				if ($row.hasClass('layout_carousel')) {
					var scrollSlides   = +$row.attr('data-scroll') || 1;
					var showSlides     = +$row.attr('data-columns') || 1;
					var doesShowDots   = $row.attr('data-dots') == 'true';

					// Media default settings
					var laptopSettings  = {
						slidesToShow: showSlides,
						slidesToScroll: scrollSlides,
						arrows: false
					};

					var padSettings     = {
						slidesToShow: showSlides,
						slidesToScroll: scrollSlides,
						arrows: false
					};

					var padMiniSettings = {
						slidesToShow: showSlides,
						slidesToScroll: scrollSlides,
						arrows: false
					};

					var mobileSettings  = {
						slidesToShow: showSlides,
						slidesToScroll: scrollSlides,
						arrows: false
					};

					// Set media queries 
					switch (showSlides) {
						case 1:
							break;

						case 2:
							mobileSettings = {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false
							};
							break;

						case 3:
							mobileSettings = {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false
							};
							break;

						case 4:
							padMiniSettings = {
								slidesToShow: 2,
								slidesToScroll: 2,
								arrows: false
							};
							mobileSettings = {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false
							};
							break;

						case 5:
							laptopSettings = {
								slidesToShow: 4,
								arrows: false,
								slidesToScroll: 4,
							};
							padSettings    = {
								slidesToShow: 3,
								slidesToScroll: 3,
								arrows: false
							};
							padMiniSettings = {
								slidesToShow: 2,
								slidesToScroll: 2,
								arrows: false
							};
							mobileSettings = {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false
							};
							break;

						case 6:
							laptopSettings = {
								slidesToShow: 5,
								slidesToScroll: 5,
								arrows: false
							};
							padSettings    = {
								slidesToShow: 4,
								slidesToScroll: 4,
								arrows: false
							};
							padMiniSettings = {
								slidesToShow: 2,
								slidesToScroll: 2,
								arrows: false
							};
							mobileSettings = {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false
							};
							break;
					}

					// Laptop slick settings
					var laptop = {
						breakpoint: 1150,
						settings: laptopSettings
					};

					// iPad slick settings
					var pad = {
						breakpoint: 992,
						settings: padSettings
					};

					// iPad Mini slick settings
					var padMini = {
						breakpoint: 768,
						settings: padMiniSettings
					};

					// Mobile slick settings
					var mobile = {
						breakpoint: 568,
						settings: mobileSettings
					};

					// General slick settings
					var values = {
						infinite: true,
						adaptiveHeight: true,
						dots: doesShowDots,
						arrows: true,
						prevArrow: '<button class="slick-arrow slick-prev"><img src="'+global_var.theme_url+'/assets/img/arrow-left.png"></button>',
						nextArrow: '<button class="slick-arrow slick-next"><img src="'+global_var.theme_url+'/assets/img/arrow-right.png"></button>',
						slidesToShow: showSlides,
						slidesToScroll: scrollSlides,
						focusOnSelect: false,
  						responsive: [
  							laptop,
  							pad,
  							padMini,
  							mobile
  						]
					};

					// Init slick slider
					$row.slick(values);
				}
				
				/**
				 * Build cases carousel
				 */
				if ($row.hasClass('row-type_cases')) {

					// General slick settings
					var values = {
						infinite: true,
						adaptiveHeight: true,
						dots: doesShowDots,
						arrows: true,
						prevArrow: '<button class="slick-arrow slick-prev"><img src="'+global_var.theme_url+'/assets/img/arrow-left.png"></button>',
						nextArrow: '<button class="slick-arrow slick-next"><img src="'+global_var.theme_url+'/assets/img/arrow-right.png"></button>',
						slidesToShow: 1,
						slidesToScroll: 1,
						focusOnSelect: false,
						adaptiveHeight: false
					};

					// Init slick slider
					$row.slick(values);

					setTimeout(function(){ $row.slick('setOption', 'height', null, true); }, 100);
				}
			});
		},

		/**
		 * After onload
		 * @return {[type]} [description]
		 */
		onLoad: function () {
			var $wrapper = $('.primary-content__wrapper');

            try {
                $wrapper.acfApi('loadAjax');
            } catch (e) {
                console.error('Load ajax error: ' + e); // pass exception object to error handler
            }
		},

		initPricingCarousel: () => {
	    // Init the second slide
	    const wrapperEl = document.getElementById('price');
	    const columnEl  = wrapperEl.querySelector('.row:last-child');
	    const newRowWrapper = document.createElement('div');

	    columnEl.remove();
	    newRowWrapper.classList = 'rows';
	    wrapperEl.appendChild(newRowWrapper);
	    newRowWrapper.appendChild(columnEl);

	    // init slick slider
	    $('#price').slick({
	      arrows: false,
	      dots:   true
	    });
		}
	};

	/** 
	 * Init method
	 */
	$.fn.primaryContent = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.primaryContent' );
        } 

    };

}));