/*  =========================
	Cookies UX
    ========================= */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {
	$(function(){
		$('#user-approve-cookies').on('click', () => {
			$('#cookies-about').slideUp('slow');
			Cookies.set('user-approve-cookies', true, { expires: 30, path: '' });
		});
	});
}));