/*  =========================
	Table */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			var $table = $(this).not('[data-inited="1"]');

			if ($table.length > 0) {
				$table.find('tr').map(function(){
					$(this).children().map(function(index){
						$(this).attr('data-label', $table.find('tr').eq(0).find('td').eq(index).text() + ':');
					});
				});
			}

			$table.attr('data-inited', 1);
		},

		tableGetter: function () {
		  const $this = $(this);

		  if (!$this.length)
		    return this;

      const request = {
        action: 'init_table_getter',
        spreadsheetId: $this.attr('data-table-id') || 0,
        worksheetName: $this.attr('data-table-name') || ''
      };

      $.ajax({
        method: 'POST',
        url: global_var.url,
        data: request,
        complete: resp => {
          const data    = JSON.parse(resp.responseText);
          const $table  = $('<table><tbody></tbody></table>');
          let emptyCells = {};

          console.dir(data);

          for (const row in data[0].dataList) {
            const $tr      = $('<tr></tr>');
            let column     = 0;
            let hasRowSpan = false;

            for (const cell in data[0].dataList[row]) {

              if (data[0].headerList[column] == cell) {
                $tr.append(`<td>${data[0].dataList[row][cell]}</td>`);
              } else {
                hasRowSpan = true;
                emptyCells[row] = cell;
              }

              column++;
            }

            $table.find('tbody').append($tr);
          }

          console.dir(emptyCells);

          $this.html($table).table('init');
        },
    	});
    }

	};

	/** 
	 * Init method
	 */
	$.fn.table = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.table' );
        } 

    };

}));