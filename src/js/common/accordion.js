/*  =========================
	Accordion */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {

	var methods = {

		/**
		 * Handle each section layout
		 */
		init: function () {
			var $accordion = $(this).not('[data-inited="1"]');

			if ($accordion.length > 0) {
				$accordion.on('click', '.accordion--title__link', function () {
					$(this).accordion('toggle');
				});
			}

			$accordion.attr('data-inited', 1);
		},

		toggle: function () {
			var $this     = $(this);
			var icon      = $this.attr('data-icon') == '-' ? '+' : '-';
			var $wrapper  = $this.closest('.accorion--item');
			var $siblings = $wrapper.siblings();
			var $noThis   = $siblings.find('.accordion--title__link');

			$wrapper.find('.accordion--content').slideToggle();
			$siblings.find('.accordion--content').slideUp();

			$noThis.attr('data-icon', '+');
			$this.attr('data-icon', icon);
		}

	};

	/** 
	 * Init method
	 */
	$.fn.accordion = function( method ) {

        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.accordion' );
        } 

    };

}));