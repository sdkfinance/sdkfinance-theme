/**
 *	Main.js
 *
 *  @package Sdk.Finance_Theme
 *  @author  Saveliy D. (dzvonkevich@gmail.com)
 */

'use strict';

/* 
	Third parts 
*/
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/slick-carousel/slick/slick.js
// ../../bower_components/pickmeup/dist/pickmeup.min.js
//= ../../bower_components/aload/dist/aload.min.js

// ../../bower_components/wow/dist/wow.min.js
//= ../../bower_components/smoothstate/jquery.smoothState.min.js
// ../../bower_components/jquery-mask-plugin/dist/jquery.mask.js
//= ../../bower_components/magnific-popup/dist/jquery.magnific-popup.js
// ../../bower_components/vivus/dist/vivus.js
// 
//= vendor/cookie-policy.js


/**
 Common functions
 */
//= common/popup.js
//= common/acf-api.js
//= common/primary-content.js
//= common/page-navigation.js
//= common/scroll-href.js
//= common/header.js
//= common/sidebar.js
//= common/accordion.js
//= common/table.js
//= common/form.js
//= common/cookie-policy.js

(function(factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }

}(function($) {

  /**
   * Methods which used main JS controller
   * @type {Object}
   */
  var methods = {


    /**
     * Use when you would like to refresh a DOM elements.
     * Used after DOM loaded and in any actions.
     */
    init: function () {

      aload();
      $(document).popup('init');
      $(document).acfApi('init');
      $(document).primaryContent('init');
      $('.page-navigation').pageNavigation('init');
      $('.scroll-href').scrollHref('init');
      $('.header-primary').header('init');
      $('.sidebar').sidebar('init');
      $('.accordion').accordion('init');
      $('table').table('init');
      $('.form').form('init');

      $('.js-popup-image').magnificPopup({
        type: 'image',
      });

      $('[data-table-id]').table('tableGetter');

      $(window).controller('addBlacklistClass');
    },

    initSmoothState: function () {
      var settings = {
        anchors: 'a',
        blacklist: '.wp-link',
        onStart: {
          duration: 280, // ms
          render: function ( $container ) {
            $(window).off();
            $container.addClass( 'slide-out' );

            $( 'body, html' ).animate( {
              scrollTop: 0,
            }, 1000);
          }
        },
        onAfter: function( $container ) {
          $container.removeClass( 'slide-out' );
          $(window).controller('init');

          var $hash = $( window.location.hash );

          if ( $hash.length !== 0 ) {
            var offsetTop = $hash.offset().top;

            $( 'body, html' ).animate( {
              scrollTop: ( offsetTop - 60 ),
            }, {
              duration: 500
            } );
          }
        }
      };

      $( '#wrapper' ).smoothState( settings );
    },

    /**
     * Add classes to link
     * which shouldn't be smooth-stated
     */
    addBlacklistClass: function () {
      $( 'a:not(.wp-link)' ).each( function() {
        var $this = $(this);

        if (
          this.href.indexOf('/wp-admin/') !== -1 ||
          this.href.indexOf('/wp-login.php') !== -1 ||
          $this.find('img').closest('.wysiwyg').length > 0 ||
          $this.attr('data-open-popup') == ''
        ) {
          $this.addClass('wp-link');
        }
      });
    }
  };

  /**
   * Include javascript files
   * which requery DOM reload
   */
  $.fn.controller = function( method ) {

    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method named ' +  method + ' isn\'t exist within jQuery.controller' );
    }

  };

  $(function(){
    $(window).controller('init');
  });

}));
