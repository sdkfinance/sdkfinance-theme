<?php
/**
 * Default sidebar template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Sdk.Finance_Theme
 */

$args = array(
	'type'         => 'post',
	'child_of'     => 0,
	'parent'       => '',
	'orderby'      => 'name',
	'order'        => 'ASC',
	'hide_empty'   => 1,
	'hierarchical' => 1,
	'exclude'      => '',
	'include'      => '',
	'number'       => 0,
	'taxonomy'     => 'category',
	'pad_counts'   => false,
	// полный список параметров смотрите в описании функции http://wp-kama.ru/function/get_terms
);
$categories = get_categories( $args );
?>

<div class="sidebar">
	<div class="sidebar-widget wysiwyg">
		<h2 class="h5">Categories</h2>
		<ul>
		<?php 
			if ( $categories )
				foreach( $categories as $cat )
					echo '<li><a href="' . get_category_link( $cat->term_id ) . '">'.$cat->name.' ('.$cat->count.')</a></li>';
		?>
		</ul>
	</div>
</div>